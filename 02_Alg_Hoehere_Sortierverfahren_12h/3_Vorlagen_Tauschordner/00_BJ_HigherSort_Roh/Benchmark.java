public abstract class Benchmark
{
    static CSV_Import_Export csv = new CSV_Import_Export();
    static int testNr=0, testNrMS=0, testNrQS=0;

    public static void benchmarkAll()
    {
        testNr++;
        int maxSize = 500000; //5000000
        int step = 10000; //100000
        int runs = 4; //20
        System.out.println("MergeSort");
        for (int size = step; size <= maxSize; size += step)
        {
            long total = 0;
            for (int i = 0; i < runs; i++)
            {
                MergeSort ms = new MergeSort(size);
                long timeStart = System.currentTimeMillis(); 
                ms.sortiere();
                long timeEnd = System.currentTimeMillis(); 
                total += (timeEnd - timeStart);
                System.gc();
            }
            System.out.println(size + "\t" + total / runs);
            schreibe("Benchmark_"+testNr, new String[]{"MergeSort",""+size,""+(total/runs)});
        }
        System.out.println("HeapSort");
        for (int size = step; size <= maxSize; size += step)
        {
            long total = 0;
            for (int i = 0; i < runs; i++)
            {
                HeapSort ms = new HeapSort(size);
                long timeStart = System.currentTimeMillis(); 
                ms.sortiere();
                long timeEnd = System.currentTimeMillis(); 
                total += (timeEnd - timeStart);
                System.gc();
            }
            System.out.println(size + "\t" + total / runs);
            schreibe("Benchmark_"+testNr, new String[]{"HeapSort",""+size,""+(total/runs)});
        }
        System.out.println("QuickSort");
        for (int size = step; size <= maxSize; size += step)
        {
            long total = 0;
            for (int i = 0; i < runs; i++)
            {
                QuickSort ms = new QuickSort(size);
                long timeStart = System.currentTimeMillis(); 
                ms.sortiere();
                long timeEnd = System.currentTimeMillis(); 
                total += (timeEnd - timeStart);
                System.gc();
            }
            System.out.println(size + "\t" + total / runs);
            schreibe("Benchmark_"+testNr, new String[]{"QuickSort",""+size,""+(total/runs)});
        }
        System.out.println("QuickSortV2");
        for (int size = step; size <= maxSize; size += step)
        {
            long total = 0;
            for (int i = 0; i < runs; i++)
            {
                QuickSortV2 ms = new QuickSortV2(size);
                long timeStart = System.currentTimeMillis(); 
                ms.sortiere();
                long timeEnd = System.currentTimeMillis(); 
                total += (timeEnd - timeStart);
                System.gc();
            }
            System.out.println(size + "\t" + total / runs);
            schreibe("Benchmark_"+testNr, new String[]{"QuickSortV2",""+size,""+(total/runs)});
        }
    }

    public static void benchmarkMerge()
    {
        testNrMS++;
        int maxSize = 10000; //5000000
        int step = 1000; //100000
        int runs = 4; //20
        System.out.println("MergeSort");
        for (int size = step; size <= maxSize; size += step)
        {
            long total = 0;
            for (int i = 0; i < runs; i++)
            {
                MergeSort ms = new MergeSort(size);
                long timeStart = System.currentTimeMillis(); 
                ms.sortiere();
                long timeEnd = System.currentTimeMillis(); 
                total += (timeEnd - timeStart);
                System.gc();
            }
            System.out.println(size + "\t" + total / runs);
            schreibe("BenchmarkMergeSort_"+testNrMS, new String[]{"MergeSort",""+size,""+(total/runs)});
        }
    }

    public static void benchmarkQuick()
    {
        testNrQS++;
        int maxSize = 10000; //5000000
        int step = 1000; //100000
        int runs = 4; //20
        System.out.println("QuickSort");
        for (int size = step; size <= maxSize; size += step)
        {
            long total = 0;
            for (int i = 0; i < runs; i++)
            {
                QuickSort qs = new QuickSort(size);
                long timeStart = System.currentTimeMillis(); 
                qs.sortiere();
                long timeEnd = System.currentTimeMillis(); 
                total += (timeEnd - timeStart);
                System.gc();
            }
            System.out.println(size + "\t" + total / runs);
            schreibe("BenchmarkQuickSort_"+testNrQS, new String[]{"QuickSort",""+size,""+(total/runs)});
        }
    }

    /**
     * Die Daten "Algorithmus", "Laenge", "Zeit" werden als datensatz uebergeben und in die
     * csv-Datei mit dem Dateinamen dateiname.csv geschrieben
     */
    private static void schreibe (String dateiName, String[] datensatz) {
        csv.writeCSV(dateiName+".csv", new String[]{"Algorithmus","Laenge","Zeit"}, datensatz, true);
    }
}