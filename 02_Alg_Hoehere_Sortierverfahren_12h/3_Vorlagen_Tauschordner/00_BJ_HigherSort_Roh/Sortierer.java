/**
 * Abstract class Sortierer - die Oberklasse aller Sortierverfahrens-Klassen
 * 
 * @author ?? Helfrich, Koch ??
 * @version 1
 */
public abstract class Sortierer extends ArrayAlgorithm
{
    public Sortierer()
    {
        super(20);
    }
    
    public Sortierer(int groesse)
    {
        super(groesse);
    }

    protected abstract void sortiere();

    public void sortiereUndMesse()
    {
        long timeStart = System.currentTimeMillis(); 
        sortiere();
        long timeEnd = System.currentTimeMillis(); 
        for (int i = 1; i < daten.length; i++)
        {
            if (daten[i-1] > daten[i]) // Fehler gefunden
            {
                System.out.println("Fehler, da nicht aufsteigend sortiert: daten[" + (i-1) + "] ist größer als daten["+i+"]!");
            }
        }
        System.out.println("AnzahlElemente: "+daten.length + "\tSortierZeit: " + (timeEnd - timeStart));
    }
}