/**
 * MergeSort implementiert den gleichnamigen Sortieralgorithmus
 * 
 * @author ?? Helfrich, Koch ??
 * @version 1
 */
public class MergeSort extends Sortierer
{
    public MergeSort()
    {
        this(10);
    }

    public MergeSort(int n)
    {
        super(n);
        setName("MergeSort");
    }

    public void sortiere()
    {
        daten = mergeSort(daten);
    }

    private double[] mergeSort(double[] a)
    {
        //# Rekursionsbasis - Array mit einem Element wird zurueckgegeben
        
        //# Aufteilung des Arrays a in zwei (fast) gleichgrosse Teilarrays
        
        //# Rekursionsschritt - das linke Teilarray sortieren
        
        //# Rekursionsschritt - das rechte Teilarray sortieren
        
        //# Mergen der sortierten Teillisten
        
        return a; // hier muss was anderes zurueckgegeben werden
    }

    public double[] merge(double[] a, double[] b)
    {
        double[] c = new double[a.length + b.length];
        int i = 0;
        int j = 0;
        int k = 0;
        //# so lange, bis eine Teilliste abgearbeitet ist
        while(i < a.length && j < b.length)
        {
            //# das jeweils kleinere Element der Teillisten wird im Array c gespeichert
            i++;
            j++;
        }
        
        //# alle restlichen Elemente werden im Array c gespeichert
        
        return c;        
    }
}