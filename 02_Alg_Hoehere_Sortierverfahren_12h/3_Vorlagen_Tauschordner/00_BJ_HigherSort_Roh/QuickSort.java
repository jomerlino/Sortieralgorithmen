/**
 * QuickSort implementiert den gleichnamigen Sortieralgorithmus
 * 
 * @author ?? Helfrich, Koch ?? veraendert Zechnall
 * @version 2
 */
public class QuickSort extends Sortierer
{
    public QuickSort(){
        this(10);   
    }

    public QuickSort(int n){
        super(n);
        setName("QuickSort");
    }

    public void sortiere()
    {
        quicksort(0, daten.length-1);
    }

    private void quicksort(int left, int right)
    {
        if (left < right)
        {
            int m = teile(left, right); //# veraendere auf teile1 / teile2 falls gewuenscht
            quicksort(left, m-1);
            quicksort(m+1, right);
        }
    }

    private int teile(int left, int right)
    {
        /*#
         * Aufgabe 1: Implementiere die Teilemethode
         * nach dem Vorgehen in der Praesentation.
         * Nutze die Hilfestellungen.
         */
        int p = right; // immer das letzte Element der Reihung wird zum Pivotelement
        int k = left;
        int g = right - 1;
        //# gehe mit k so lange nach rechts bis Wert (daten[k]) groesser als Pivotwert (daten[p])
        //# k muss immer kleiner oder gleich g+1 sein!

        //# gehe mit g so lange nach links bis Wert (daten[g]) kleiner gleich dem Pivotwert (daten[p])
        //# g muss immer groesser oder gleich als k sein!

        //# wiederhole, solange k kleiner g
        while (k < g)
        {
            //# tausche k und g

            //# gehe erneut mit beiden Zeigern k und g

        }
        //# tausche am Ende k und g

        // gib k zurueck
        return k;
    }

    private int teile1(int left, int right)
    {
        /*#
         * Aufgabe 1*: Implementiere die Teilemethode
         * nach dem Vorgehen in der Praesentation.
         * Ganz ohne Hilfestellungen.
         */

        return -1; //# muss geaendert werden
    }

    protected int teile2(int left, int right) // wird fuer QuickSortV2 genutzt
    {
        /*#
         * Aufgabe 2***: Versuche diese Alternative einer Teile-Methode
         * nachzuvollziehen.
         */
        int p = right;
        int m = left;
        for (int i = left; i < right; i++)
        {
            if (daten[i] <= daten[p])
            {
                tausche(i, m);
                m++;
            }
        }
        tausche(m, right);
        return m;
    }
}