import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * Die Test-Klasse Speicher.
 *
 * @author  (Ihr Name)
 * @version (eine Versionsnummer oder ein Datum)
 */
public class Speicher
{
    private QuickSort quickSor1;
    private MergeSort mergeSor1;

    /**
     * Konstruktor fuer die Test-Klasse Speicher
     */
    public Speicher()
    {
    }

    /**
     *  Setzt das Testgerüst fuer den Test.
     *
     * Wird vor jeder Testfall-Methode aufgerufen.
     */
    @Before
    public void setUp()
    {
        //# Anzahl der zu sortienden Zahlen:
        int anzahl = 10;

        //# Teste MergeSort - ggf. auskommentieren!
        mergeSor1 = new MergeSort(anzahl);
        mergeSor1.ausgeben();
        mergeSor1.sortiere();
        mergeSor1.ausgeben(0,anzahl-1);

        //# Teste QuickSort - ggf. auskommentieren!
        // quickSor1 = new QuickSort(anzahl);
        // quickSor1.ausgeben();
        // quickSor1.sortiere();
        // quickSor1.ausgeben(0,anzahl-1);
    }

    /**
     * Gibt das Testgerüst wieder frei.
     *
     * Wird nach jeder Testfall-Methode aufgerufen.
     */
    @After
    public void tearDown()
    {
    }
}