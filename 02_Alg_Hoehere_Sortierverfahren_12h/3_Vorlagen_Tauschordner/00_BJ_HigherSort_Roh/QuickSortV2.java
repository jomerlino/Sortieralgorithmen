/**
 * QuickSortV2
 *
 * @author ?? Helfrich, Koch ??
 * @version 1
 */
public class QuickSortV2 extends QuickSort
{
    public QuickSortV2()
    {
        super(10);
    }

    public QuickSortV2(int n)
    {
        super(n);
    }

    public void sortiere()
    {
        quicksort(0, daten.length);
    }

    private void quicksort(int left, int right)
    {
        /*#
         * Aufgabe ***: Diese Quicksort-Variante ist effizienter. Versuche
         * den Algorithmus nachzuvollziehen.
         */
        while(right - left > 10)
        {
            int m = teile2(left, right);
            if (m - left > right - (m+1)) // links groesser -> rechts Rekursion
            {
                quicksort(m+1, right);
                right = m;
            }
            else
            {
                quicksort(left, m);
                left = m+1;
            }
        }
        for (int i = left+1; i < right; i++)
        {
            double tmp = daten[i];
            int j = i-1;
            while(j >= left && daten[j] > tmp)
            {
                daten[j+1] = daten[j];
                j--;
            }
            daten[j+1] = tmp;
        }
    }
}