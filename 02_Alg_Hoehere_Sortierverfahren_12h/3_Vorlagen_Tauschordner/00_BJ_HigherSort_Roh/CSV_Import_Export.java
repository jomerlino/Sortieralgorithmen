import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.File;
import java.io.FileWriter;
import com.csvreader.CsvReader;
import com.csvreader.CsvWriter;

/**
 * Die Klasse CSV_Import_Export ist die Benutzerschnittstelle zum .csv-Export und -Import.
 * 
 * @author Zechnall
 * @version 11.05.2011
 */
public class CSV_Import_Export
{
    /**
     * readCSV
     * 
     * @param   String  dateiName - ist der Name der datei, in die geschrieben werden soll
     */
    public void readCSV(String dateiName) {
        try {

            CsvReader datei = new CsvReader(dateiName);

            datei.readHeaders();

            while (datei.readRecord())
            {
                String productID = datei.get("ProductID");
                String productName = datei.get("ProductName");

                // perform program logic here
                System.out.println(productID + ": " + productName);
            }

            datei.close();

        } catch (FileNotFoundException e) {
            System.out.println("Die Datei mit dem Dateinamen "+dateiName+" wurde nicht gefunden!");
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /**
     * writeCSV
     * 
     * @param   String  dateiName - ist der Name der Datei, in die geschrieben werden soll
     * @param   String[]  attribute - die Attribute
     * @param   String[]  datensatz - der Datensatz
     * @param   boolean  append - gibt an, ob die Daten hinzugefuegt werden sollen oder ob eine neue Datei
     *          geschrieben werden soll
     */
    public void writeCSV(String dateiName, String[] attribute, String[] datensatz, boolean append) {
        // Pfad zum Unterverzeichnis "Benchmarks" definieren
        String verzeichnis = "Benchmarks/";
        String datei = verzeichnis + dateiName; // Pfad zur Datei im Unterverzeichnis

        // Überprüfen, ob das Unterverzeichnis existiert, und falls nicht, dieses erstellen
        File dir = new File(verzeichnis);
        if (!dir.exists()) {
            dir.mkdirs(); // Erstellt das Verzeichnis, falls es nicht existiert
        }

        // Überprüfen, ob die Datei bereits existiert
        boolean alreadyExists = new File(datei).exists();

        try {
            // Verwenden des FileWriter-Konstruktors, der das Anhängen an die Datei spezifiziert
            CsvWriter csvOutput = new CsvWriter(new FileWriter(datei, append), ',');

            // Wenn die Datei noch nicht existiert, schreiben wir die Kopfzeile
            if (!alreadyExists) {
                for (String item : attribute) csvOutput.write(item);
                csvOutput.endRecord();
            }
            // Annahme: Wenn die Datei bereits existiert, enthält sie bereits die korrekte Kopfzeile

            // Einige Datensätze schreiben
            for (String item : datensatz) csvOutput.write(item);
            csvOutput.endRecord();

            // Schließen des CsvWriters
            csvOutput.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}