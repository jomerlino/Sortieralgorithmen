/**
 * Quantil
 * 
 * @author ?? Helfrich, Koch ??
 * @version 1
 */
public class Quantil extends ArrayAlgorithm
{
    double[] datenSortiert;

    public Quantil(int n)
    {
        super(n);
        datenSortiert = java.util.Arrays.copyOf(daten, daten.length);
        java.util.Arrays.sort(datenSortiert);
    }

    public double findQuantilAndCheck(double perc)
    {
        int q = (int)(perc*daten.length);
        long timeStart = System.currentTimeMillis(); 
        double dQ = findQuantil(0, daten.length-1, q);
        long timeEnd = System.currentTimeMillis(); 
        if (datenSortiert[q] != dQ)
        {
            System.out.println("Fehler: Quantil sollte " + datenSortiert[q] + " sein, gefunden: " + dQ + "!");
        }
        else System.out.println("Super: Quantil "+dQ+" gefunden bei "+daten.length + " Elementen in " + (timeEnd - timeStart) + " ms");
        return dQ;
    }
    
    public double findQuantil(int left, int right, int q)
    {
        /*#
         * Implementiere die Methode findQuantil.
         * Rufe zum Ueberpruefen der Implementierung die Methode findQuantilAndCheck(p) auf.
         */
        
        return 0;
    }

    private int teile(int left, int right)
    {
        int p = right;
        int m = left;
        for (int i = left; i < right; i++)
        {
            if (daten[i] <= daten[p])
            {
                tausche(i, m);
                m++;
            }
        }
        tausche(m, right);
        return m;
    }
}