/**
 * Abstract class ArrayAlgorithm - die Oberklasse aller Algos auf Arrays
 * 
 * @author ?? Helfrich, Koch ??
 * @version 1
 */
public abstract class ArrayAlgorithm
{
    protected double[] daten;
    private double[] origDaten;
    private String name;

    public ArrayAlgorithm(int groesse)
    {
        // System.out.print('\u000C'); // zum loeschen der Konsolenausgabe!
        daten = new double[groesse];
        origDaten = new double[groesse];
        java.util.Random r = new java.util.Random();
        for (int i = 0; i < groesse; i++)
        {
            daten[i] = r.nextDouble()*1000;
            origDaten[i] = daten[i];
        }
    }

    public ArrayAlgorithm(double[] uebergebeneDaten)
    {
        daten = uebergebeneDaten;
        origDaten = new double[uebergebeneDaten.length];
        for (int i = 0; i < uebergebeneDaten.length; i++)
        {
            origDaten[i] = daten[i];
        }
    }

    public void zuruecksetzen()
    {
        for (int i = 0; i < daten.length; i++)
        {
            daten[i] = origDaten[i];
        }
    }
    
    public void setData(double[] d){
        daten = d;
    }
    
    public double[] getData(){
        return daten;
    }

    public void ausgeben(int von, int bis)
    {
        System.out.println(name+"============================================");
        for (int i = Math.max(0,von); i < daten.length && i <= bis; i++)
        {
            System.out.println("daten["+i+"] = " + daten[i]);
        }
        System.out.println("====================================================");
    }

    public void ausgeben()
    {
        System.out.println(name+"============================================");
        for (int i = 0; i < 5 && i < daten.length; i++)
        {
            System.out.println("daten["+i+"] = " + daten[i]);
        }
        if (daten.length > 10)
        {
            System.out.println("...");
        }
        for (int i = Math.max(5, daten.length-5); i < daten.length; i++)
        {
            System.out.println("daten["+i+"] = " + daten[i]);
        }
        System.out.println("====================================================");
    }
    
    protected void setName(String neuerName){
        name = neuerName;
    }

    /**
     * @param x die 1. Stelle des Tauschfeldes
     * @param y die 2. Stelle des Tauschfeldes
     */
    protected void tausche(int x, int y)
    {
        double t = daten[x];
        daten[x] = daten[y];
        daten[y] = t;
    }
}