import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.util.Random;
import java.util.Arrays;

/**
 * Die Test-Klasse MergeSortTest.
 *
 * @author  (Ihr Name)
 * @version (eine Versionsnummer oder ein Datum)
 */
public class MergeSortTest
{
    private MergeSort ms;
    private final Random random = new Random();

    /**
     * Konstruktor fuer die Test-Klasse MergeSortTest
     */
    public MergeSortTest()
    {
        ms = new MergeSort();
    }

    /**
     *  Setzt das Testgerüst fuer den Test.
     *
     * Wird vor jeder Testfall-Methode aufgerufen.
     */
    @BeforeEach
    public void setUp()
    {
    }

    /**
     * Gibt das Testgerüst wieder frei.
     *
     * Wird nach jeder Testfall-Methode aufgerufen.
     */
    @AfterEach
    public void tearDown()
    {
    }

    @Test
    public void testMergeBothNonEmptyAllAlessThanB() {
        double[] a = {1.0, 2.0, 3.0};
        double[] b = {4.0, 5.0, 6.0};
        double[] expected = {1.0, 2.0, 3.0, 4.0, 5.0, 6.0};
        double[] result = ms.merge(a, b);
        assertArrayEquals(expected, result);
        System.out.println("The arrays should merge correctly when all elements in a are less than those in b.");
    }

    @Test
    public void testMergeBothNonEmptyMixedElements() {
        double[] a = {1.0, 3.0, 5.0};
        double[] b = {2.0, 4.0, 6.0};
        double[] expected = {1.0, 2.0, 3.0, 4.0, 5.0, 6.0};
        double[] result = ms.merge(a, b);
        assertArrayEquals(expected, result);
        System.out.println("The arrays should merge correctly with mixed elements.");
    }

    @Test
    public void testMergeOneEmpty() {
        double[] a = {};
        double[] b = {1.0, 2.0, 3.0};
        double[] expected = {1.0, 2.0, 3.0};
        double[] result = ms.merge(a, b);
        assertArrayEquals(expected, result);
        System.out.println("The arrays should merge correctly when one array is empty.");
    }

    @Test
    public void testMergeBothEmpty() {
        double[] a = {};
        double[] b = {};
        double[] expected = {};
        double[] result = ms.merge(a, b);
        assertArrayEquals(expected, result);
        System.out.println("The arrays should merge correctly when both arrays are empty.");
    }

    @Test
    public void testMergeWithRandomSortedArrays() {
        // Generate two random-sized, sorted arrays
        double[] a = generateAndSortRandomArray();
        double[] b = generateAndSortRandomArray();

        // Merge the arrays
        double[] mergedArray = ms.merge(a, b);

        // Verify the merged array is sorted
        assertTrue(isSorted(mergedArray));
        System.out.println("The merged array should be sorted. Found: " + Arrays.toString(mergedArray));
    }
    
    @Test
    public void testMergeSort() {
        // Generate a random array
        double[] randomArray = generateRandomArray(100); // Generate an array with 100 elements, for example

        // Set the random array to be sorted
        ms.setData(randomArray); // Assuming a method setData to set the array

        // Perform the sorting
        ms.sortiere();

        // Retrieve the sorted array
        double[] sortedArray = ms.getData(); // Assuming a method getData to retrieve the array

        // Verify the sorted array is indeed sorted
        assertTrue(isSorted(sortedArray));
        System.out.println("The array should be sorted.");
    }
    
    // Helper method to generate a random array of doubles
    private double[] generateRandomArray(int size) {
        double[] array = new double[size];
        for (int i = 0; i < size; i++) {
            array[i] = random.nextDouble() * 100; // Fill the array with random doubles up to 100
        }
        return array;
    }

    private double[] generateAndSortRandomArray() {
        int size = random.nextInt(100) + 1; // Generate arrays of size 1 to 100
        double[] array = new double[size];
        for (int i = 0; i < size; i++) {
            array[i] = random.nextDouble() * 100; // Fill with random doubles up to 100
        }
        Arrays.sort(array); // Sort the array
        return array;
    }

    private boolean isSorted(double[] array) {
        for (int i = 0; i < array.length - 1; i++) {
            if (array[i] > array[i + 1]) {
                return false; // Found an element that is greater than the next one
            }
        }
        return true;
    }
}