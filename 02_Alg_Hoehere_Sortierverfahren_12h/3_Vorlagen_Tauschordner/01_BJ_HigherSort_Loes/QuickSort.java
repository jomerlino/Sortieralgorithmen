/**
 * QuickSort implementiert den gleichnamigen Sortieralgorithmus
 * 
 * @author ?? Helfrich, Koch ??
 * @version 1
 */
public class QuickSort extends Sortierer
{
    public QuickSort(){
        this(10);
    }
    
    public QuickSort(int n){
        super(n);
        setName("QuickSort");
    }

    public void sortiere()
    {
        quicksort(0, daten.length-1);
    }

    private void quicksort(int left, int right)
    {
        if (left < right)
        {
            int m = teile(left, right);
            quicksort(left, m-1);
            quicksort(m+1, right);
        }
    }

    private int teile(int left, int right)
    {
        int p = right; // immer das letzte Element der Reihung wird zum Pivotelement
        int k = left;
        int g = right - 1;
        // gehe mit k so lange nach rechts bis Wert groesser als Pivot
        // k muss immer kleiner oder gleich g+1 sein!
        while (daten[k] <= daten[p] && k <= g) k++;
        // gehe mit g so lange nach links bis Wert kleiner gleich dem Pivotwert
        // g muss immer groesser oder gleich als k sein!
        while (daten[g] > daten[p] && g > k) g--;
        // wiederhole, solange k kleiner g
        while (k < g)
        {
            // tausche k und g
            tausche(k, g);
            // gehe erneut mit beiden Zeigern k und g
            while (daten[k] <= daten[p] && k <= g) k++;
            while (daten[g] > daten[p] && g > k) g--;
        }
        // tausche am Ende k und g
        tausche(k, p);
        // gib k zurueck
        return k;
    }

    private int teile1(int left, int right)
    {
        int p = right; // immer das letzte Element der Reihung wird zum Pivotelement
        int k = left;
        int g = right - 1;
        while (k < g)
        {
            while (daten[k] <= daten[p] && k <= g) k++;
            while (daten[g] > daten[p] && g > k) g--;
            if (k < g) tausche(k, g); // k<g, da bei k>g falsch getauscht wird
        }
        // bei nur noch zwei Elementen in der Teilmenge darf nur getauscht werden,
        // wenn daten[k] > daten[p]!
        if (daten[k] > daten[p]) tausche(k, p); 
        return k;
    }

    protected int teile2(int left, int right) // wird fuer QuickSortV2 genutzt
    {
        int p = right;
        int m = left;
        for (int i = left; i < right; i++)
        {
            if (daten[i] <= daten[p])
            {
                tausche(i, m);
                m++;
            }
        }
        tausche(m, right);
        return m;
    }
}