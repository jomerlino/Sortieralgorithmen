import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.util.Random;

/**
 * Die Test-Klasse HeapSortTest.
 *
 * @author  (Ihr Name)
 * @version (eine Versionsnummer oder ein Datum)
 */
public class HeapSortTest
{
    private final Random random = new Random();
    private HeapSort hs;

    /**
     * Konstruktor fuer die Test-Klasse HeapSortTest
     */
    public HeapSortTest()
    {
    }

    /**
     *  Setzt das Testgerüst fuer den Test.
     *
     * Wird vor jeder Testfall-Methode aufgerufen.
     */
    @BeforeEach
    public void setUp()
    {
        hs = new HeapSort();
    }

    /**
     * Gibt das Testgerüst wieder frei.
     *
     * Wird nach jeder Testfall-Methode aufgerufen.
     */
    @AfterEach
    public void tearDown()
    {
    }
    
    @Test
    public void testSortWithRandomArray() {
        // Generate a random array
        double[] randomArray = generateRandomArray(100); // Generate an array with 100 elements, for example

        // Set the random array to be sorted
        hs.setData(randomArray); // Assuming a method setData to set the array

        // Perform the sorting
        hs.sortiere();

        // Retrieve the sorted array
        double[] sortedArray = hs.getData(); // Assuming a method getData to retrieve the array

        // Verify the sorted array is indeed sorted
        assertTrue(isSorted(sortedArray));
        System.out.println("The array should be sorted.");
    }

    // Helper method to generate a random array of doubles
    private double[] generateRandomArray(int size) {
        double[] array = new double[size];
        for (int i = 0; i < size; i++) {
            array[i] = random.nextDouble() * 100; // Fill the array with random doubles up to 100
        }
        return array;
    }

    // Helper method to check if an array is sorted
    private boolean isSorted(double[] array) {
        for (int i = 0; i < array.length - 1; i++) {
            if (array[i] > array[i + 1]) {
                return false; // If any element is greater than the next, the array is not sorted
            }
        }
        return true;
    }
}