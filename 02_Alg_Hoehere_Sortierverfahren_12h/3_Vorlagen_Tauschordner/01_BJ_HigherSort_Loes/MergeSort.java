/**
 * MergeSort implementiert den gleichnamigen Sortieralgorithmus
 * 
 * @author ?? Helfrich, Koch ??
 * @version 1
 */
public class MergeSort extends Sortierer
{
    public MergeSort()
    {
        this(10);
    }
    
    public MergeSort(int n)
    {
        super(n);
        setName("MergeSort");
    }

    public void sortiere()
    {
        daten = mergeSort(daten);
    }

    private double[] mergeSort(double[] a)
    {
        if (a.length <= 1)
        {
            return a;
        }
        double[] links = new double[a.length / 2];
        double[] rechts = new double[a.length - links.length];
        for (int i = 0; i < links.length; i++)
        {
            links[i] = a[i];
        }
        for (int i = links.length; i < a.length; i++)
        {
            rechts[i-links.length] = a[i];
        }
        links = mergeSort(links);
        rechts = mergeSort(rechts);
        return merge(links, rechts);
    }

    public double[] merge(double[] a, double[] b)
    {
        double[] c = new double[a.length + b.length];
        int i = 0;
        int j = 0;
        while(i < a.length && j < b.length)
        {
            if (a[i] <= b[j])
            {
                c[i+j] = a[i];
                i++;
            }
            else
            {
                c[i+j] = b[j];
                j++;
            }
        }
        while(i < a.length)
        {
            c[i+j] = a[i];
            i++;
        }
        while(j < b.length)
        {
            c[i+j] = b[j];
            j++;
        }
        return c;        
    }
}