/**
 * HeapSort implementiert den gleichnamigen Sortieralgorithmus
 * 
 * @author ?? Helfrich, Koch ??
 * @version 1
 */
public class HeapSort extends Sortierer
{
    public HeapSort()
    {
        this(10);
    }
    
    public HeapSort(int n)
    {
        super(n);
        setName("HeapSort");
    }

    public void sortiere()
    {
        int end = daten.length;
        Heapify(end);
        end--;
        while(end > 0)
        {
            tausche(0, end);
            end--;
            ShiftDown(0, end);
        }
    }

    private void Heapify(int n)
    {
        int start = (n-2)/2;
        while(start >= 0)
        {
            ShiftDown(start, n-1);
            start--;
        }
    }

    private void ShiftDown(int start, int end)
    {
        int root = start;
        while(root*2+1 <= end)
        {
            int child = 2*root+1;
            if (child + 1 <= end && daten[child] < daten[child+1])
                child++;
            if (daten[root] < daten[child])
            {
                tausche(root, child);
                root = child;
            }
            else
                return;
        }
    }
}