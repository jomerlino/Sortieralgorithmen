/**
 * QuickSortV2
 *
 * @author ?? Helfrich, Koch ??
 * @version 1
 */
public class QuickSortV2 extends QuickSort
{
    public QuickSortV2(int n)
    {
        super(n);
        setName("QuickSortV2");
    }

    public void sortiere()
    {
        quicksort(0, daten.length-1);
    }

    private void quicksort(int left, int right)
    {
        while(right > left + 2 + 21) // Mindestens 11 Elemente sortieren
        {
            int m = teile2(left, right);
            if (m - left > right - m) // links groesser -> rechts Rekursion
            {
                quicksort(m+1, right);
                right = m-1;
            }
            else // rechts groesser/gleich -> links Rekursion
            {
                quicksort(left, m-1);
                left = m+1;
            }
        }
        for (int i = left+1; i <= right; i++)
        {
            double tmp = daten[i];
            int j = i-1;
            while(j >= left && daten[j] > tmp)
            {
                daten[j+1] = daten[j];
                j--;
            }
            daten[j+1] = tmp;
        }
    }
}