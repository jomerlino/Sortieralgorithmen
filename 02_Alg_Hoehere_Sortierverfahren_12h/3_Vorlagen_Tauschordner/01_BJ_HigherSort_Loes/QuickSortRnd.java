/**
 * QuickSortRnd implementiert den gleichnamigen Sortieralgorithmus
 * 
 * @author Zechnall
 * @version 1
 */
public class QuickSortRnd extends QuickSort
{
    public QuickSortRnd(int n){
        super(n);
        setName("QuickSortRnd");
    }

    public void sortiere()
    {
        quicksort(0, daten.length-1);
    }

    private void quicksort(int left, int right)
    {
        if (left < right)
        {
            int m = teile(left, right);
            quicksort(left, m-1);
            quicksort(m+1, right);
        }
    }
    
    private int teile(int left, int right)
    {
        //# Zufaelliges Pivotelement wird gewuerfelt
        int r = left + (int)(Math.random()*((right-left)+1));
        //# Damit es nicht im Weg ist, wird es nach right getauscht
        tausche(r, right);
        //# Ab jetzt wie gewohnt!
        int p = right;
        int m = left;
        for (int i = left; i < right; i++)
        {
            if (daten[i] <= daten[p])
            {
                tausche(i, m);
                m++;
            }
        }
        tausche(m, right);
        return m;
    }
}