/**
 * Quantil
 * 
 * @author ?? Helfrich, Koch ??, Zechnall
 * @version 1
 */
public class Quantil extends ArrayAlgorithm
{
    double[] datenSortiert;

    public Quantil()
    {
        this(10);
    }
    
    public Quantil(int n)
    {
        super(n);
        datenSortiert = java.util.Arrays.copyOf(daten, daten.length);
        java.util.Arrays.sort(datenSortiert);
    }
    
    public Quantil(double[] uebergebeneDaten)
    {
        super(uebergebeneDaten);
    }

    /*
     * @param double perc Prozent aus dem Intervall [0;1] 
     */
    public double findQuantilAndCheck(double perc)
    {
        int q = (int)(perc*daten.length);
        long timeStart = System.currentTimeMillis(); 
        double dQ = findQuantil(0, daten.length-1, q);
        long timeEnd = System.currentTimeMillis(); 
        if (datenSortiert[q] != dQ)
        {
            System.out.println("Fehler: Quantil sollte " + datenSortiert[q] + " sein, gefunden: " + dQ + "!");
        }
        else System.out.println("Super: Quantil "+dQ+" gefunden bei "+daten.length + " Elementen in " + (timeEnd - timeStart) + " ms");
        return dQ;
    }
    
    public double findQuantil(int left, int right, int q)
    {
        while (left < right){
            int m = teile(left, right);
            if(m == q) return daten[m];
            if(m < q) left = m+1;
            else right = m-1; // (m > q)
        }
        return daten[left]; // left >= right
    }
    
    public int findQuantilPos(int left, int right, int q)
    {
        while (left < right){
            int m = teile(left, right);
            if(m == q) return m;
            if(m < q) left = m+1;
            else right = m-1; // (m > q)
        }
        return left; // left >= right
    }

    private int teile(int left, int right)
    {
        int p = right;
        int m = left;
        for (int i = left; i < right; i++)
        {
            if (daten[i] <= daten[p])
            {
                tausche(i, m);
                m++;
            }
        }
        tausche(m, right);
        return m;
    }
}