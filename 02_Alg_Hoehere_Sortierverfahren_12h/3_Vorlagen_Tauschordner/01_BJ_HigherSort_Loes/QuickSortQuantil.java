/**
 * QuickSortQuantil implementiert den gleichnamigen Sortieralgorithmus
 * 
 * @author Zechnall
 * @version 1
 */
public class QuickSortQuantil extends QuickSort
{
    Quantil q;

    public QuickSortQuantil(int n){
        super(n);
        setName("QuickSortQuantil");
        q = new Quantil(daten);
    }

    public void sortiere()
    {
        //# Quantiles Pivotelement wird gesucht
        int qPos = q.findQuantilPos(0, daten.length-1, (int)(0.5*daten.length));
        //# Damit es als Pivotel. dient, wird es nach daten.length-1 getauscht
        tausche(qPos, daten.length-1);
        quicksort(0, daten.length-1);
    }

    private void quicksort(int left, int right)
    {
        if (left < right)
        {
            int m = teile(left, right);
            quicksort(left, m-1);
            quicksort(m+1, right);
        }
    }

    private int teile(int left, int right)
    {
        int p = right;
        int m = left;
        for (int i = left; i < right; i++)
        {
            if (daten[i] <= daten[p])
            {
                tausche(i, m);
                m++;
            }
        }
        tausche(m, right);
        return m;
    }
}
