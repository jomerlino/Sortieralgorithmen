/**
 * Beschreiben Sie hier die Klasse Mergen.
 * 
 * @author Uwe Seckinger
 * @version 2018.04.06
 */
import java.util.ArrayList;
public abstract class MergenAbstr   {

    /**
     * Die Methode merge verschmilzt zwei Listen miteinander, indem das jeweils kleinere Element zuerst
     * in die neue Liste eingetragen wird. Unter der Voraussetzung, dass links und rechts aufsteigend 
     * sortiert waren, ist es auch die Ergebnisliste
     * 
     * @param   links sortierte Liste
     * @param   rechts sortierte Liste
     * @return  aufsteigend sortierte Liste bestehend aus den Elementen von links und rechts
     */
    public ArrayList< Integer > merge(ArrayList< Integer > links, ArrayList< Integer > rechts)   {
        //    Erzeuge eine neue leere Liste
        //    while(solange die linke und die rechte Listen nicht leer sind){
        //       if(erstes Element der linken Liste <= erstes Element der rechten Liste){
        //          Füge das erste Element der linken Liste
        //          ans Ende der neuen Liste ein
        //          Entferne das Element aus der linken Liste
        //       }else{
        //          Füge das erste Element der rechten Liste in
        //          ans Ende der neuen ein
        //          Entferne das Element aus der rechten Liste
        //       }
        //    }
        //    while(solange die linke Liste nicht leer ist){
        //       Füge das erste Element der linken Liste
        //       ans Ende der neuen Liste ein
        //       Entferne das Element aus der linken Liste
        //    }
        //    while(solange die rechte Liste nicht leer ist){
        //       Füge das erste Element der rechten Liste
        //       ans Ende der neuen Liste ein
        //       Entferne das Element aus der rechten Liste
        //    }
        return links; //links wird als dummy verwendet damit die Methode uebersetzt werden kann. Die neue Liste muss zurueckgegeben werden.
    }
}


