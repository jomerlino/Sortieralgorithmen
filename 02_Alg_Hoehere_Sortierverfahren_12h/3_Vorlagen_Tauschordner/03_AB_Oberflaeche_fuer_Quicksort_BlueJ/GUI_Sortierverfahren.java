import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JTextArea;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JScrollPane;
import javax.swing.JLabel;

/**
 * @author Matthias Zimmer und Uwe Seckinger
 * @version 2017.04.20
 * Oberfläche zur Wiedergabe von Sortierergebnissen 
 *
 */
public class GUI_Sortierverfahren extends JFrame {

    private JPanel contentPane;
    private JTextField textFieldAnzahlDatensaetze;
    private JTextArea textAreaDatensaetze;
    private Datenverwaltung datenverwaltung;
    private JScrollPane scrollPane;
    private JLabel lblSortierdauer;
    private JTextField textFieldZuSuchenderDatensatz;
    private JLabel lblAusgabeDatensatzSuche0;
    private JLabel lblAusgabeDatensatzSuche1;
    private JLabel lblAnzahlVergleiche ;
    private int bt_tfHoehe=23;
    private int btAbst=11;
    private int anzBt=5;

    /**
     * Create the frame.
     */
    public GUI_Sortierverfahren() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 453, 510+anzBt*34);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);

        textFieldAnzahlDatensaetze = new JTextField();
        textFieldAnzahlDatensaetze.setText("5");
        textFieldAnzahlDatensaetze.setBounds(10, 11, 131, bt_tfHoehe);
        contentPane.add(textFieldAnzahlDatensaetze);
        textFieldAnzahlDatensaetze.setColumns(10);

        JButton btnDatensaetzeErzeugen = new JButton("zuf\u00E4llige Datens\u00E4tze erzeugen");
        btnDatensaetzeErzeugen.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent arg0) {
                    datenverwaltung.zufaelligeDatensaetzeErzeugen(Integer.parseInt(textFieldAnzahlDatensaetze.getText()));
                    datensaetzeAusgeben();
                }
            });
        btnDatensaetzeErzeugen.setBounds(151, 10, 290, bt_tfHoehe);
        contentPane.add(btnDatensaetzeErzeugen);

        JButton btnDatensaetzeMitWiederholung = new JButton("Datens\u00E4tze mit Wiederholungen erzeugen");
        btnDatensaetzeMitWiederholung.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent arg0) {
                    datenverwaltung.datensaetzeMitWiederholungErzeugen(Integer.parseInt(textFieldAnzahlDatensaetze.getText()));
                    datensaetzeAusgeben();
                }
            });
        btnDatensaetzeMitWiederholung.setBounds(151, 42, 290, bt_tfHoehe);
        contentPane.add(btnDatensaetzeMitWiederholung);

        JButton btnDatensaetzeVorgegeben = new JButton("Vorgegebene Datens\u00E4tze erzeugen");
        btnDatensaetzeVorgegeben.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent arg0) {
                    datenverwaltung.datensaetzeVorgeben();
                    datensaetzeAusgeben();
                }
            });
        btnDatensaetzeVorgegeben.setBounds(151, 74, 290, bt_tfHoehe);
        contentPane.add(btnDatensaetzeVorgegeben);

        JButton btnQuickMittesort = new JButton("Quicksort Pivot in der Mitte durchf\u00FChren");
        btnQuickMittesort.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    datenverwaltung.sortierenPivotMitte();
                    datensaetzeAusgeben();
                }
            });
        btnQuickMittesort.setBounds(10, 104, 431, bt_tfHoehe);
        contentPane.add(btnQuickMittesort);

        JButton btnQuickLinksSort = new JButton("Quicksort Pivot links durchf\u00FChren");
        btnQuickLinksSort.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    datenverwaltung.sortierenPivot0();
                    datensaetzeAusgeben();
                }
            });
        btnQuickLinksSort.setBounds(10, 104+bt_tfHoehe+btAbst, 431, bt_tfHoehe);
        contentPane.add(btnQuickLinksSort);

        JButton btnQuickRechtsSort = new JButton("Quicksort Pivot rechts durchf\u00FChren");
        btnQuickRechtsSort.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    datenverwaltung.sortierenPivotRechts();
                    datensaetzeAusgeben();
                }
            });
        btnQuickRechtsSort.setBounds(10, 104+2*(bt_tfHoehe+btAbst), 431, bt_tfHoehe);
        contentPane.add(btnQuickRechtsSort);       

        JButton btnQuickOptSort = new JButton("Quicksort optimiertes Pivot durchf\u00FChren");
        btnQuickOptSort.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    datenverwaltung.sortierenPivotOpt();
                    datensaetzeAusgeben();
                }
            });
        btnQuickOptSort.setBounds(10, 104+3*(bt_tfHoehe+btAbst), 431, bt_tfHoehe);
        contentPane.add(btnQuickOptSort); 

        lblSortierdauer = new JLabel("Sortierdauer: 0 s ");
        lblSortierdauer.setBounds(10, 128+(anzBt-1)*(+bt_tfHoehe+btAbst), 431, 14);
        contentPane.add(lblSortierdauer);

        lblAnzahlVergleiche = new JLabel("Anzahl Vergleiche:  0");
        lblAnzahlVergleiche.setBounds(10, 145+(anzBt-1)*(+bt_tfHoehe+btAbst), 431, 14);
        contentPane.add(lblAnzahlVergleiche);

        scrollPane = new JScrollPane();
        scrollPane.setBounds(10, 166+(anzBt-1)*(+bt_tfHoehe+btAbst), 431, 337);
        contentPane.add(scrollPane);

        textAreaDatensaetze = new JTextArea();
        textAreaDatensaetze.setEditable(false);
        scrollPane.setViewportView(textAreaDatensaetze);

        // Instanz der Datenverwaltung erzeugen
        datenverwaltung = new Datenverwaltung();

        this.setVisible(true);
    }

    // Gibt die Datensätze aus dem Objekt datenverwaltung aus
    private void datensaetzeAusgeben() {
        // Ausgabefeld leeren
        textAreaDatensaetze.setText("");
        // Datensätze ausgeben
        int[] dummy=datenverwaltung.getDaten();
        for (int i = 0; i < datenverwaltung.getDaten().length; i++) {
            textAreaDatensaetze.append(datenverwaltung.getDaten()[i] + "\n");
        }
        // Zeitdauer setzen
        lblSortierdauer.setText("Sortierdauer: " + datenverwaltung.getLaufzeit() + " ms");

        // Anzahl Vergleiche setzen
        lblAnzahlVergleiche.setText("Anzahl Vergleiche: " + datenverwaltung.getAnzahlVergleiche());
    }

    public Datenverwaltung getDV(){
        return datenverwaltung;
    }
}
