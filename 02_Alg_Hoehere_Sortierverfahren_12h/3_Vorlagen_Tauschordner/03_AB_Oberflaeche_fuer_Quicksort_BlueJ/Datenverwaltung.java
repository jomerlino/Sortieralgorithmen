/**
 * @author Matthias Zimmer und Uwe Seckinger
 * @version 2017.06.14
 * Fachklasse zur Wiedergabe von Sortierergebnissen 
 *
 */

public class Datenverwaltung {

    private int[] daten;  //Array, das die zu sortierenden Daten enthält
    private long anzahlVergleiche = 0;  //Variable, speichert die Anzahl der Vergleiche gespeichert werden kann. Inhalt wird automatisch von Oberfläche übernommen.
    // Variable, speichert in die die Laufzeit des Algorithmus in Millisekunden Wert wird automatisch in die Oberfläche übernommen.
    private long laufzeit = 0;

    // Methode, die die Elemente an den Stellen ersterIndex und zweiterIndex vertauscht 
    private void tauscheElementeAnPositionen(int ersterIndex, int zweiterIndex) {
        int zwischenspeicher = daten[ersterIndex];         // Element an erster Position in Zwischenspeicher merken
        daten[ersterIndex] = daten[zweiterIndex];        // Element an zweiter Position an erste Position kopieren 
        daten[zweiterIndex] = zwischenspeicher;         // Element aus Zwischenspeicher in zweite Position kopieren
    }

    /**
     * Sortiert die Daten im Array int[] daten mit Hilfe von QuickSort
     */
    public void sortierenPivotMitte() {
        long startzeit = System.currentTimeMillis();        // Für Laufzeitüberprüfung Startzeitpunkt ermitteln.
        anzahlVergleiche = 0;        // Anzahl Vergleich auf 0 setzen
        quicksortMitte(0,getDaten().length-1);            //rekursiver Aufruf für rechte Teilfolge
        laufzeit = (System.currentTimeMillis() - startzeit);
    }

    /** rekursive Sortierung eines Abschnitts von Index lo bis hi
     *  der Reihung daten */
    private void quicksortMitte (int lo, int hi) {
        int li=lo, re=hi;        
        int x = daten[(lo+hi)/2];       
        while (li<=re) {
            anzahlVergleiche++;
            while (daten[li]<x) {li++; anzahlVergleiche++;}    
            while (daten[re]>x) {re--; anzahlVergleiche++;}              
            if (li<=re) {
                tauscheElementeAnPositionen(li, re);
                li++; re--;
            }
        }
        if (lo<re) { quicksortMitte(lo, re); }
        if (li<hi) { quicksortMitte(li, hi); }
    }

    public void sortierenPivot0() {
        long startzeit = System.currentTimeMillis();        // Für Laufzeitüberprüfung Startzeitpunkt ermitteln.
        anzahlVergleiche = 0;        // Anzahl Vergleich auf 0 setzen
        quicksort0(0,getDaten().length-1);            //rekursiver Aufruf für rechte Teilfolge
        laufzeit = (System.currentTimeMillis() - startzeit);
    }

    /** rekursive Sortierung eines Abschnitts von Index lo bis hi
     *  der Reihung daten */
    private void quicksort0 (int lo, int hi) {
        int li=lo, re=hi;        
        int x = daten[li];       
        while (li<=re) {    
            anzahlVergleiche++;
            while (daten[li]<x) {li++; anzahlVergleiche++;}    
            while (daten[re]>x) {re--; anzahlVergleiche++;}              
            if (li<=re) {
                tauscheElementeAnPositionen(li, re);
                li++; re--;
            }
        }
        if (lo<re) { quicksort0(lo, re); }
        if (li<hi) { quicksort0(li, hi); }
    }

    public void sortierenPivotRechts() {
        long startzeit = System.currentTimeMillis();        // Für Laufzeitüberprüfung Startzeitpunkt ermitteln.
        anzahlVergleiche = 0;        // Anzahl Vergleich auf 0 setzen
        quicksortr(0,getDaten().length-1);            //rekursiver Aufruf für rechte Teilfolge
        laufzeit = (System.currentTimeMillis() - startzeit);
    }

    /** rekursive Sortierung eines Abschnitts von Index lo bis hi
     *  der Reihung daten */
    private void quicksortr (int lo, int hi) {
        int li=lo, re=hi;        
        int x = daten[re];       
        while (li<=re) {    
            anzahlVergleiche++;
            while (daten[li]<x) {li++; anzahlVergleiche++;}    
            while (daten[re]>x) {re--; anzahlVergleiche++;}              
            if (li<=re) {
                tauscheElementeAnPositionen(li, re);
                li++; re--;
            }
        }
        if (lo<re) { quicksortr(lo, re); }
        if (li<hi) { quicksortr(li, hi); }
    }
    
        public void sortierenPivotOpt() {
        long startzeit = System.currentTimeMillis();        // Für Laufzeitüberprüfung Startzeitpunkt ermitteln.
        anzahlVergleiche = 0;        // Anzahl Vergleich auf 0 setzen
        quicksorto(0,getDaten().length-1);            //rekursiver Aufruf für rechte Teilfolge
        laufzeit = (System.currentTimeMillis() - startzeit);
    }

    /** rekursive Sortierung eines Abschnitts von Index lo bis hi
     *  der Reihung daten */
    private void quicksorto (int lo, int hi) {
        int li=lo, re=hi;        
        int x = daten[0];       //hier optimieren
        while (li<=re) {    
            anzahlVergleiche++;
            while (daten[li]<x) {li++; anzahlVergleiche++;}    
            while (daten[re]>x) {re--; anzahlVergleiche++;}              
            if (li<=re) {
                tauscheElementeAnPositionen(li, re);
                li++; re--;
            }
        }
        if (lo<re) { quicksorto(lo, re); }
        if (li<hi) { quicksorto(li, hi); }
    }

    /**********************************************************
     * 
     * AB HIER NICHTS MEHR ÄNDERN!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
     * 
     **********************************************************/

    /**
     * stellt das Datensatzarray für die Oberfläche bereit
     * 
     * @return Array mit Datensätzen
     */
    public int[] getDaten() {
        return daten;
    }

    /**
     * stellt die Anzahl der Vergleiche für die Oberfläche bereit
     * 
     * @return Bei Algorithmus ermittelte Anzahl der Vergleiche
     */
    public long getAnzahlVergleiche() {
        return anzahlVergleiche;
    }

    /**
     * stellt die Laufzeit des Algorithmus für Oberfläche bereit
     * 
     * @return Bei Algorithmus ermittelte Laufzeit
     */
    public long getLaufzeit() {
        return  laufzeit;
    }

    /**
     * Füllt das Array daten mit Zufallszahlen zwischen 0 und der Zehnfachen
     * Anzahl
     * 
     * @param Anzahl der zu erzeugende Datensätze
     */
    public void zufaelligeDatensaetzeErzeugen(int anzahl) {
        daten = new int[anzahl];
        for (int i = 0; i < anzahl; i++) {
            daten[i] = (int) (Math.random() * anzahl * 10);
        }
    }

    /**
     * Füllt das Array daten mit Zufallszahlen zwischen 0 und der Hälfte der
     * Anzahl -> häufige Wiederholungen
     * 
     * @param Anzahl der zu erzeugende Datensätze
     */
    public void datensaetzeMitWiederholungErzeugen(int anzahl) {
        daten = new int[anzahl];
        for (int i = 0; i < anzahl; i++) {
            daten[i] = (int) (Math.random() * anzahl / 2);
        }
    }

    /**
     * Füllt das Array daten mit Zufallszahlen zwischen 0 und der Hälfte der
     * Anzahl -> häufige Wiederholungen
     * 
     * @param Anzahl der zu erzeugende Datensätze
     */
    public void datensaetzeVorgeben() {
        //         daten=new int[] {5,3,7,1,2,4,6,8};
        daten=new int[] {21,16,77,12,33,20,14,15,88,23,25,66};
        // daten=new int[] {1,2,3,4,5,6,7,8};
    }
}
