/**
 * @author Matthias Zimmer und Uwe Seckinger
 * @version 2018.04.06
 * Fachklasse zur Wiedergabe von Sortierergebnissen 
 *
 */
import java.util.ArrayList;
public class DatenverwaltungListen {

    private ArrayList<Integer> daten;  //Array, das die zu sortierenden Daten enthält
    private long anzahlVergleiche = 0;  //Variable, speichert die Anzahl der Vergleiche gespeichert werden kann. Inhalt wird automatisch von Oberfläche übernommen.
    // Variable, speichert in die die Laufzeit des Algorithmus in Millisekunden Wert wird automatisch in die Oberfläche übernommen.
    private long laufzeit = 0;
//     private Mergen verschmelzen;

    public DatenverwaltungListen () {
//         verschmelzen=new Mergen();
        daten = new ArrayList<Integer>();
    }
    // Methode, die die Elemente an den Stellen ersterIndex und zweiterIndex vertauscht 
    private void tauscheElementeAnPositionen(int ersterIndex, int zweiterIndex) {
        int zwischenspeicher = daten.get(ersterIndex);      // Element an erster Position in Zwischenspeicher merken
        daten.set((ersterIndex),daten.get(zweiterIndex));       // Element an zweiter Position an erste Position kopieren 
        daten.set(zweiterIndex,zwischenspeicher);         // Element aus Zwischenspeicher in zweite Position kopieren
    }

    public void sortierenMerge() {
        long startzeit = System.currentTimeMillis();        // Für Laufzeitüberprüfung Startzeitpunkt ermitteln.
        anzahlVergleiche = 0;       // Anzahl Vergleich auf 0 setzen
        daten=sort(daten);          //rekursiver Aufruf für rechte Teilfolge
        laufzeit = (System.currentTimeMillis() - startzeit);
    }

    /** rekursive Sortierung der ArrayList daten
     */
    public ArrayList<Integer> sort(ArrayList<Integer> arrayl)  {
//         if (Abbruchbedingung) {
//            // Bestimme die Mitte
             ArrayList<Integer> links = new ArrayList<Integer>();
//             links befüllen

             ArrayList<Integer> rechts = new ArrayList<Integer>();
//             rechts befüllen;
// 
//              Rekursion
// 
//             return merge(links, rechts);
//         }   else    {
             return arrayl;
//         }
    }

    public ArrayList< Integer > merge(ArrayList< Integer > links, ArrayList< Integer > rechts)   {
        ArrayList< Integer > neueArrayl = new ArrayList< Integer >();

        while (!links.isEmpty() && !rechts.isEmpty()) {
            if (links.get(0) < rechts.get(0)) {
                neueArrayl.add(links.get(0));
                links.remove(0);
            } else {
                neueArrayl.add(rechts.get(0));
                rechts.remove(0);
            }
        }

        while (links.size()>0) {
            neueArrayl.add(links.get(0));
            links.remove(0);
        }

        while (rechts.size()>0) {
            neueArrayl.add(rechts.get(0));
            rechts.remove(0);
        }

        return neueArrayl;
    }

    /**********************************************************
     * 
     * AB HIER NICHTS MEHR ÄNDERN!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
     * 
     **********************************************************/

    /**
     * stellt das Datensatzarray für die Oberfläche bereit
     * 
     * @return Array mit Datensätzen
     */
    public ArrayList< Integer > getDaten() {
        return daten;
    }

    /**
     * stellt die Anzahl der Vergleiche für die Oberfläche bereit
     * 
     * @return Bei Algorithmus ermittelte Anzahl der Vergleiche
     */
    public long getAnzahlVergleiche() {
        return anzahlVergleiche;
    }

    /**
     * stellt die Laufzeit des Algorithmus für Oberfläche bereit
     * 
     * @return Bei Algorithmus ermittelte Laufzeit
     */
    public long getLaufzeit() {
        return  laufzeit;
    }

    /**
     * Füllt das Array daten mit Zufallszahlen zwischen 0 und der Zehnfachen
     * Anzahl
     * 
     * @param Anzahl der zu erzeugende Datensätze
     */
    public void zufaelligeDatensaetzeErzeugen(int anzahl) {
        for (int i = 0; i < anzahl; i++) {
            daten.add((int)(Math.random() * anzahl *10));
        }
    }

    /**
     * Füllt das Array daten mit Zufallszahlen zwischen 0 und der Hälfte der
     * Anzahl -> häufige Wiederholungen
     * 
     * @param Anzahl der zu erzeugende Datensätze
     */
    public void datensaetzeMitWiederholungErzeugen(int anzahl) {
        for (int i = 0; i < anzahl; i++) {
            daten.add((int)(Math.random() * anzahl / 2));
        }
    }
}
