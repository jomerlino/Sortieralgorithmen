import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JTextArea;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JScrollPane;
import javax.swing.JLabel;

/**
 * @author Matthias Zimmer und Uwe Seckinger
 * @version 2017.04.20
 * Oberfläche zur Wiedergabe von Sortierergebnissen 
 *
 */
public class GUI_Sortierverfahren extends JFrame {

    private JPanel contentPane;
    private JTextField textFieldAnzahlDatensaetze;
    private JTextArea textAreaDatensaetze;
    private Datenverwaltung datenverwaltung;
    private JScrollPane scrollPane;
    private JLabel lblSortierdauer;
    private JLabel lblAnzahlVergleiche ;
    private int bt_tfHoehe=23;
    private int btAbst=11;
    private int anzBt=3;

    /**
     * Create the frame.
     */
    public GUI_Sortierverfahren() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 453, 510+anzBt*34);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);

        textFieldAnzahlDatensaetze = new JTextField();
        textFieldAnzahlDatensaetze.setText("5");
        textFieldAnzahlDatensaetze.setBounds(10, 11, 131, bt_tfHoehe);
        contentPane.add(textFieldAnzahlDatensaetze);
        textFieldAnzahlDatensaetze.setColumns(10);

        JButton btnDatensaetzeErzeugen = new JButton("zuf\u00E4llige Datens\u00E4tze erzeugen");
        btnDatensaetzeErzeugen.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent arg0) {
                    datenverwaltung.zufaelligeDatensaetzeErzeugen(Integer.parseInt(textFieldAnzahlDatensaetze.getText()));
                    datensaetzeAusgeben();
                }
            });
        btnDatensaetzeErzeugen.setBounds(151, 10, 290, bt_tfHoehe);
        contentPane.add(btnDatensaetzeErzeugen);

        JButton btnDatensaetzeMitWiederholung = new JButton("Datens\u00E4tze mit Wiederholungen erzeugen");
        btnDatensaetzeMitWiederholung.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent arg0) {
                    datenverwaltung
                    .datensaetzeMitWiederholungErzeugen(Integer.parseInt(textFieldAnzahlDatensaetze.getText()));
                    datensaetzeAusgeben();
                }
            });
        btnDatensaetzeMitWiederholung.setBounds(151, 40, 290, bt_tfHoehe);
        contentPane.add(btnDatensaetzeMitWiederholung);

        JButton btnDatensaetzeZeichen = new JButton("Zeichen-Datens\u00E4tze erzeugen");
        btnDatensaetzeZeichen.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent arg0) {
                    datenverwaltung
                    .datensaetzeZeichenErzeugen(Integer.parseInt(textFieldAnzahlDatensaetze.getText()));
                    datensaetzeAusgeben();
                }
            });
        btnDatensaetzeZeichen.setBounds(151, 70, 290, bt_tfHoehe);
        contentPane.add(btnDatensaetzeZeichen);

        JButton btnSelectionsort = new JButton("Radixsort (dez) durchf\u00FChren");
        btnSelectionsort.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    datenverwaltung.radix10Sort();
                    datensaetzeAusgeben();
                }
            });
        btnSelectionsort.setBounds(10, 100, 431, bt_tfHoehe);
        contentPane.add(btnSelectionsort);

        JButton btnBubbleSort = new JButton("Radixsort (bin) durchf\u00FChren");
        btnBubbleSort.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    datenverwaltung.radix2Sort();
                    datensaetzeAusgeben();
                }
            });
        btnBubbleSort.setBounds(10, 100+bt_tfHoehe+btAbst, 431, bt_tfHoehe);
        contentPane.add(btnBubbleSort);

        JButton btnInsertionSort = new JButton("Radixsort (A-Z) durchf\u00FChren");
        btnInsertionSort.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    datenverwaltung.radixAZSort();
                    datensaetzeAusgeben();
                }
            });
        btnInsertionSort.setBounds(10, 100+2*(+bt_tfHoehe+btAbst), 431, bt_tfHoehe);
        contentPane.add(btnInsertionSort);
        
        scrollPane = new JScrollPane();
        scrollPane.setBounds(10, 166+(anzBt-1)*(+bt_tfHoehe+btAbst), 431, 337);
        contentPane.add(scrollPane);

        textAreaDatensaetze = new JTextArea();
        textAreaDatensaetze.setEditable(false);
        scrollPane.setViewportView(textAreaDatensaetze);

        // Instanz der Datenverwaltung erzeugen
        datenverwaltung = new Datenverwaltung();

        this.setVisible(true);
    }

    // Gibt die Datensätze aus dem Objekt datenverwaltung aus
    private void datensaetzeAusgeben() {
        // Ausgabefeld leeren
        textAreaDatensaetze.setText("");
        // Datensätze ausgeben
        for (int i = 0; i < datenverwaltung.getDaten().length; i++) {
            if (datenverwaltung.istZeichen()) {
                textAreaDatensaetze.append((char)(datenverwaltung.getDaten()[i]+65) + "\n");
            }
            else {
                textAreaDatensaetze.append(datenverwaltung.getDaten()[i] + "\n");
            }
        }
    }

    public Datenverwaltung getDV(){
        return datenverwaltung;
    }
}
