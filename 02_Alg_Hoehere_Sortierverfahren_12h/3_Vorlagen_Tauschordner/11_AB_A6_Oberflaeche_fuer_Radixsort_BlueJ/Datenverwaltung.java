import java.util.*;
public class Datenverwaltung {

    private ZahlZeichen[] daten;  //Array, das die zu sortierenden Daten enthält
    private long anzahlVergleiche = 0;  //Variable, speichert die Anzahl der Vergleiche gespeichert werden kann. Inhalt wird automatisch von Oberfläche übernommen.
    // Variable, speichert in die die Laufzeit des Algorithmus in Millisekunden Wert wird automatisch in die Oberfläche übernommen.
    private long laufzeit = 0;
    private int maxStellenZahl2;

    // Methode, die die Elemente an den Stellen ersterIndex und zweiterIndex vertauscht 
    private void tauscheElementeAnPositionen(int ersterIndex, int zweiterIndex) {
        ZahlZeichen zwischenspeicher = daten[ersterIndex]; 		// Element an erster Position in Zwischenspeicher merken
        daten[ersterIndex] = daten[zweiterIndex];		// Element an zweiter Position an erste Position kopieren 
        daten[zweiterIndex] = zwischenspeicher; 		// Element aus Zwischenspeicher in zweite Position kopieren
    }

    /**
     * sortiert die Daten im Array int[] daten mit Hilfe von SelectionSort
     * Verwendet wird das Bin&auml;rsystem
     */
    public void radixSort1() {
        int zaehler=0;
        ArrayList[]   fachPart = new ArrayList[2];            // die 10 Faecher
        for(int i=0;i<2;i++) {
            fachPart[i]=new ArrayList<ZahlZeichen>(); // eine ArrayList fuer jedes Fach
        }
        int fach;
        for (int i=0; i<maxStellenZahl2; i++) {            // Schleife ueber alle Fächer
            int teiler=(int)(Math.pow(2,i));
            // Partitionierungsphase: teilt die Daten auf die Faecher auf
            for (int j=0; j<daten.length; j++) {
                zaehler++;
                fach=(daten[j].getZahl()/teiler)%2;  // ermittelt die Fachnummer: 0 bis 1                   
                fachPart[fach].add(daten[j]);   // kopiert j-tes Element ins richtige Fach
            }
            // Sammelphase: kopiert die beiden Faecher wieder zusammen
            int pos=0;
            for (int j=0;j<2;j++)   {
                for(int k=0;k<fachPart[j].size();k++) {
                    zaehler++;
                    daten[pos]=(ZahlZeichen)(fachPart[j].get(k));
                    pos++;
                }
                fachPart[j].clear();
            }
        }    
    }

    /**
     * sortiert die Daten im Array int[] daten mit Hilfe von RadixSort
     */
    public void radixSort2() {
        long startzeit = System.currentTimeMillis();		// Für Laufzeitüberprüfung Startzeitpunkt ermitteln.
        anzahlVergleiche = 0;		// Anzahl Vergleich auf 0 setzen
        // Implementierung RadixSort
        laufzeit = (System.currentTimeMillis() - startzeit);       // Laufzeit berechnen
    }

    /**********************************************************
     * 
     * AB HIER NICHTS MEHR ÄNDERN!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
     * 
     ********************************************************** 
     */

    /**
     * stellt das Datensatzarray für die Oberfläche bereit
     * 
     * @return Array mit Datensätzen
     */
    public ZahlZeichen[] getDaten() {
        return daten;
    }

    /**
     * stellt die Anzahl der Vergleiche für die Oberfläche bereit
     * 
     * @return Bei Algorithmus ermittelte Anzahl der Vergleiche
     */
    public long getAnzahlVergleiche() {
        return anzahlVergleiche;
    }

    /**
     * stellt die Laufzeit des Algorithmus für Oberfläche bereit
     * 
     * @return Bei Algorithmus ermittelte Laufzeit
     */
    public int getLaufzeit() {
        return (int) laufzeit;
    }

    /**
     * Füllt das Array daten mit Zufallszahlen zwischen 0 und der Hälfte der
     * Anzahl -> häufige Wiederholungen
     * 
     * @param Anzahl
     *            der zu erzeugende Datensätze
     */
    public void datensaetzeMitWiederholungErzeugen(int anzahl) {
        daten = new ZahlZeichen[anzahl];
        for (int i = 0; i < anzahl; i++) {
            daten[i] = new ZahlZeichen(anzahl/2);
        }
        maxStellenZahl2=(int)(Math.log(anzahl)/Math.log(2));
    }
}
