/**
 * @author Matthias Zimmer und Uwe Seckinger
 * @version 2018.04.06
 * Fachklasse zur Wiedergabe von Sortierergebnissen 
 *
 */

public class Datenverwaltung {

    private int[] daten;  //Array, das die zu sortierenden Daten enthält
    private long anzahlVergleiche = 0;  //Variable, speichert die Anzahl der Vergleiche gespeichert werden kann. Inhalt wird automatisch von Oberfläche übernommen.
    // Variable, speichert in die die Laufzeit des Algorithmus in Millisekunden Wert wird automatisch in die Oberfläche übernommen.
    private long laufzeit = 0;
    private Mergen verschmelzen;
    
    public Datenverwaltung () {
        verschmelzen=new Mergen();
    }
    // Methode, die die Elemente an den Stellen ersterIndex und zweiterIndex vertauscht 
    private void tauscheElementeAnPositionen(int ersterIndex, int zweiterIndex) {
        int zwischenspeicher = daten[ersterIndex];      // Element an erster Position in Zwischenspeicher merken
        daten[ersterIndex] = daten[zweiterIndex];       // Element an zweiter Position an erste Position kopieren 
        daten[zweiterIndex] = zwischenspeicher;         // Element aus Zwischenspeicher in zweite Position kopieren
    }

    public void sortierenMerge() {
        long startzeit = System.currentTimeMillis();        // Für Laufzeitüberprüfung Startzeitpunkt ermitteln.
        anzahlVergleiche = 0;       // Anzahl Vergleich auf 0 setzen
        daten=sort(daten);          //rekursiver Aufruf für rechte Teilfolge
        laufzeit = (System.currentTimeMillis() - startzeit);
    }

    /** rekursive Sortierung der Reihung daten
     */
    public int[] sort(int[] array)  {
        if (array.length > 1) {
            int mitte = (int)(array.length / 2);

            int[] links = new int[mitte];
            for (int i = 0; i <= links.length - 1; i++) {
                links[i] = array[i];
            }

            int[] rechts = new int[array.length - mitte];
            for (int i = mitte; i <= array.length - 1; i++) {
                rechts[i - mitte] = array[i];
            }

            links = sort(links);
            rechts = sort(rechts);

            return verschmelzen.merge(links, rechts);
        }   else    {
            return array;
        }
    }


    /**********************************************************
     * 
     * AB HIER NICHTS MEHR ÄNDERN!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
     * 
     **********************************************************/

    /**
     * stellt das Datensatzarray für die Oberfläche bereit
     * 
     * @return Array mit Datensätzen
     */
    public int[] getDaten() {
        return daten;
    }

    /**
     * stellt die Anzahl der Vergleiche für die Oberfläche bereit
     * 
     * @return Bei Algorithmus ermittelte Anzahl der Vergleiche
     */
    public long getAnzahlVergleiche() {
        return anzahlVergleiche;
    }

    /**
     * stellt die Laufzeit des Algorithmus für Oberfläche bereit
     * 
     * @return Bei Algorithmus ermittelte Laufzeit
     */
    public long getLaufzeit() {
        return  laufzeit;
    }

    /**
     * Füllt das Array daten mit Zufallszahlen zwischen 0 und der Zehnfachen
     * Anzahl
     * 
     * @param Anzahl der zu erzeugende Datensätze
     */
    public void zufaelligeDatensaetzeErzeugen(int anzahl) {
        daten = new int[anzahl];
        for (int i = 0; i < anzahl; i++) {
            daten[i] = (int) (Math.random() * anzahl * 10);
        }
    }

    /**
     * Füllt das Array daten mit Zufallszahlen zwischen 0 und der Hälfte der
     * Anzahl -> häufige Wiederholungen
     * 
     * @param Anzahl der zu erzeugende Datensätze
     */
    public void datensaetzeMitWiederholungErzeugen(int anzahl) {
        daten = new int[anzahl];
        for (int i = 0; i < anzahl; i++) {
            daten[i] = (int) (Math.random() * anzahl / 2);
        }
    }
}
