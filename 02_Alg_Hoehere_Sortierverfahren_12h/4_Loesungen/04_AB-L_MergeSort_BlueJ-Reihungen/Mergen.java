
/**
 * Beschreiben Sie hier die Klasse Mergen.
 * 
 * @author (Ihr Name) 
 * @version (eine Versionsnummer oder ein Datum)
 */
public class Mergen   {
    // Instanzvariablen - ersetzen Sie das folgende Beispiel mit Ihren Variablen
    private int x;

    /**
     * Ein Beispiel einer Methode - ersetzen Sie diesen Kommentar mit Ihrem eigenen
     * 
     * @param  y    ein Beispielparameter für eine Methode
     * @return        die Summe aus x und y
     */
    public int[] merge(int[] links, int[] rechts)   {
        int[] neueArray = new int[links.length + rechts.length];
        int indexLinks = 0;
        int indexRechts = 0;
        int indexErgebnis = 0;

        while (indexLinks < links.length && indexRechts < rechts.length) {
            if (links[indexLinks] < rechts[indexRechts]) {
                neueArray[indexErgebnis] = links[indexLinks];
                indexLinks += 1;
            } else {
                neueArray[indexErgebnis] = rechts[indexRechts];
                indexRechts += 1;
            }
            indexErgebnis += 1;
        }

        while (indexLinks < links.length) {
            neueArray[indexErgebnis] = links[indexLinks];
            indexLinks += 1;
            indexErgebnis += 1;
        }

        while (indexRechts < rechts.length) {
            neueArray[indexErgebnis] = rechts[indexRechts];
            indexRechts += 1;
            indexErgebnis += 1;
        }

        return neueArray;
    }
}
