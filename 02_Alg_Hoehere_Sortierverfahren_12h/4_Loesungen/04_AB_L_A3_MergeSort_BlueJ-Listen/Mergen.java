/**
 * Beschreiben Sie hier die Klasse Mergen.
 * 
 * @author Uwe Seckinger
 * @version 2018.04.06
 */
import java.util.ArrayList ;
public class Mergen extends MergenAbstr  {

    /**
     * Die Methode merge verschmilzt zwei Listen miteinander, indem das jeweils kleinere Element zuerst
     * in die neue Liste eingetragen wird. Unter der Voraussetzung, dass links und rechts aufsteigend 
     * sortiert waren, ist es auch die Ergebnisliste
     * 
     * @param   links sortierte Liste
     * @param   rechts sortierte Liste
     * @return  aufsteigend sortierte Liste bestehend aus den Elementen von links und rechts
     */
    public ArrayList< Integer > merge(ArrayList< Integer > links, ArrayList< Integer > rechts)   {
        ArrayList< Integer > neueListe=new ArrayList< Integer >();
        while(!links.isEmpty() && !rechts.isEmpty())  {
            if(links.get(0)< rechts.get(0)){
                neueListe.add(links.get(0));
                links.remove(0);
            }else{
                neueListe.add(rechts.get(0));
                rechts.remove(0);
            }
        }
        while(!links.isEmpty()){
            neueListe.add(links.get(0));
            links.remove(0);
        }
        while(!rechts.isEmpty()){
            neueListe.add(rechts.get(0));
            rechts.remove(0);
        }
        return neueListe; //links wird als dummy verwendet damit die Methode uebersetzt werden kann. Die neue Liste muss zurueckgegeben werden.
    }
}
