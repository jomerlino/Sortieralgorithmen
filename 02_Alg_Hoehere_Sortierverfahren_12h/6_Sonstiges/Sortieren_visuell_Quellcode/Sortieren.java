import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import java.util.*;

/**
  *
  * Beschreibung
  *
  * @version 1.0 vom 18.11.2009
  * @author Hans-Christian M�ller
  */

public class Sortieren extends JFrame {
  // Anfang Attribute
  int[] feld;
  private JButton jButton1 = new JButton();
  Graphics g;
  Graphics2D g2;
  ZeichenThread t;
  boolean threadGestartet;
  private JPanel jPanel1 = new JPanel(null);
  private JLabel jLabel1 = new JLabel();
  private JSlider jSlider1 = new JSlider();
  private JLabel jLabel2 = new JLabel();
  private JButton jButton2 = new JButton();
    private String[] jComboBox1Data = {"Minsort", "Bubblesort", "Quicksort"};
  private JComboBox jComboBox1 = new JComboBox(jComboBox1Data);
  // Ende Attribute

  public Sortieren(String title) {
    // Frame-Initialisierung
    super(title);
    setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    int frameWidth = 737; 
    int frameHeight = 435;
    setSize(frameWidth, frameHeight);
    Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
    int x = (d.width - getSize().width) / 2;
    int y = (d.height - getSize().height) / 2;
    setLocation(x, y);
    Container cp = getContentPane();
    cp.setLayout(null);
    // Anfang Komponenten

    jButton1.setBounds(120, 360, 137, 33);
    jButton1.setText("Start / Pause");
    jButton1.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        jButton1_ActionPerformed(evt);
      }
    });
    jButton1.setFont(new Font("Tahoma", Font.PLAIN, 11));
    cp.add(jButton1);
    jPanel1.setBounds(8, 8, 713, 337);
    jPanel1.setBackground(Color.WHITE);
    cp.add(jPanel1);
    jLabel1.setBounds(440, 368, 36, 17);
    jLabel1.setText("schnell");
    jLabel1.setFont(new Font("Tahoma", Font.PLAIN, 11));
    cp.add(jLabel1);
    jSlider1.setBounds(488, 355, 153, 41);
    jSlider1.setMinimum(1);
    jSlider1.setValue(100);
    jSlider1.setMaximum(200);
    jSlider1.addChangeListener(new ChangeListener() {
      public void stateChanged(ChangeEvent evt) {
        jSlider1_StateChanged(evt);
      }
    });
    cp.add(jSlider1);
    jLabel2.setBounds(656, 368, 43, 17);
    jLabel2.setText("langsam");
    jLabel2.setFont(new Font("Tahoma", Font.PLAIN, 11));
    cp.add(jLabel2);
    jButton2.setBounds(272, 360, 137, 33);
    jButton2.setText("Stopp / Neustart");
    jButton2.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        jButton2_ActionPerformed(evt);
      }
    });
    cp.add(jButton2);
    jComboBox1.setBounds(16, 368, 89, 21);
    jComboBox1.setSelectedIndex(2);
    jComboBox1.setFont(new Font("Tahoma", Font.PLAIN, 11));
    cp.add(jComboBox1);
    // Ende Komponenten
    setResizable(false);
    setVisible(true);
    g = getGraphics();
    g2 = (Graphics2D)g;
    g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
    feldBelegen();
    timerInit();
  }

  // Anfang Methoden

  public void feldBelegen() {
    feld = new int[100];
    for (int i=0; i<feld.length; i++)
      feld[i] = (int)(Math.random()*295+5);
  }
  
  public void timerInit() {
    t = new ZeichenThread(g2, feld);
    t.setzeTempo(2*jSlider1.getValue());
    t.setzeVerfahren(jComboBox1.getSelectedIndex());
    t.warte(80);
    t.zeichneFeld();
    //t.warte(1000);
    threadGestartet = false;
  }
  
  public void jButton1_ActionPerformed(ActionEvent evt) {
    t.setzeTempo(2*jSlider1.getValue());
    t.setzeVerfahren(jComboBox1.getSelectedIndex());
    jComboBox1.setEnabled(false);
    
    if (!threadGestartet) {
       t.start();
       threadGestartet = true;
    }
    
    else {
      if (!t.istAktiv()) {
        synchronized (t) {
          t.proceed();
        }
      }

      else {
        synchronized (t) {
          t.pause();
        }
      }
    }
  }

  public void jSlider1_StateChanged(ChangeEvent evt) {
    t.setzeTempo(2*jSlider1.getValue());
  }

  public void jButton2_ActionPerformed(ActionEvent evt) {
    if (t.isAlive()) t.stop();
    feldBelegen();
    jComboBox1.setEnabled(true);
    timerInit();
  }

  // Ende Methoden


  public static void main(String[] args) {
    try {
      // Set System L&F
      UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
    }
    catch (Exception e) {}
    
    new Sortieren("Sortieren");
  }
}



