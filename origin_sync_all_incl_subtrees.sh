#!/bin/bash

# Skriptname: origin_sync_all_incl_subtrees.sh
# Beschreibung: Synchronisiert das Hauptrepository und alle Subtrees (erst Pull, dann Push)

# Activate Git Credential Cache
git config --global credential.helper 'cache --timeout=3600'

# Check if we're in the root directory of a Git repository
if [ ! -d .git ]; then
    echo "Dieses Skript muss im Wurzelverzeichnis eines Git-Repositories ausgeführt werden."
    exit 1
fi

# Switch to main branch
if git show-ref --verify --quiet refs/heads/main; then
    git checkout main || {
        echo "Error: Failed to checkout main branch."
        exit 1
    }
else
    echo "Error: main branch does not exist."
    exit 1
fi
echo "<<<<<<<<  Switched to local main branch.  >>>>>>>"

# Prompt user for a commit message
echo "Geben Sie eine Commit-Nachricht ein (drücken Sie Enter für die Standardnachricht):"
read user_commit_message </dev/tty

# Set default commit message if none is provided
commit_message="${user_commit_message:-Automatischer Commit am $(date +"%Y-%m-%d um %H:%M:%S")}"

# 1. Pull in main repository
echo "Pull im Hauptrepository..."
git pull origin main

# 2. Stage and commit all changes
echo "Füge alle Änderungen hinzu..."
git add .

echo "Committe Änderungen mit Nachricht: '$commit_message'"
git commit -m "$commit_message"

# Check if the commit was successful
if [ $? -ne 0 ]; then
    echo "Keine Änderungen zum Committen vorhanden oder ein Fehler ist aufgetreten."
else
    echo "Änderungen erfolgreich committet."
fi

# 3. Push to main repository
echo "Pushe Änderungen ins Hauptrepository..."
git push origin main

# 4. Synchronize subtrees
subtrees=$(git config --file .git/config --get-regexp 'subtree\..*\.prefix' | awk '{print $1}' | sed 's/subtree\.//g' | sed 's/\.prefix//g')

if [ -z "$subtrees" ]; then
    echo "Keine Subtrees im aktuellen Repository gefunden."
    exit 0
fi

echo "Gefundene Subtrees: $subtrees"

# Synchronize each subtree
for subtree in $subtrees; do
    # Retrieve subtree details
    prefix=$(git config --file .git/config --get subtree.$subtree.prefix)
    repo=$(git config --file .git/config --get subtree.$subtree.url)
    branch=$(git config --file .git/config --get subtree.$subtree.branch)
    branch="${branch:-main}"

    echo "--------------------------------------------"
    echo "Synchronisiere Subtree: $subtree"
    echo "Pfad: $prefix"
    echo "Repository: $repo"
    echo "Branch: $branch"
    echo "--------------------------------------------"

    # Pull subtree changes
    git subtree pull --prefix="$prefix" "$repo" "$branch" --squash -m "Subtree $subtree aktualisiert"

    # Check if the pull was successful
    if [ $? -ne 0 ]; then
        echo "Fehler beim Aktualisieren des Subtrees $subtree."
        exit 1
    fi

    # Commit subtree changes
    git add "$prefix"
    git commit -m "Subtree $subtree aktualisiert"

    # Push subtree changes
    git subtree push --prefix="$prefix" "$repo" "$branch"

    # Check if the push was successful
    if [ $? -ne 0 ]; then
        echo "Fehler beim Pushen des Subtrees $subtree."
        exit 1
    fi

    echo "Subtree $subtree erfolgreich synchronisiert."
done

echo "Alle Subtrees wurden synchronisiert."

echo "Das Hauptrepository und alle Subtrees wurden synchronisiert."

exit 0