/**
 * Beschreiben Sie hier die Klasse Testroutine.
 * 
 * @author Dirk Zechnall, veraendert Thomas Schaller 23.05.2022
 * @version 11.05.2011
 */
import java.util.Random;

public class Testroutine
{
    // Instanzvariablen - ersetzen Sie das folgende Beispiel mit Ihren Variablen
    private CSV_Import_Export csv;
    private SortingAlgorithm sort;
    private String[] attribute = {"Algorithmus","TestlaufNr","AnzahlElemente","Dauer","Vergleiche","Vertauschungen"};
    private String[] datensatz = new String[6];
    private int zaehler;

    /**
     * Konstruktor f�r Objekte der Klasse Testroutine
     */
    public Testroutine(SortingAlgorithm sa)
    {
        csv = new CSV_Import_Export();
        sort = sa;
    }

    public void test(){
        test(3, 10000, 50000);
    }

    /**
     * Die Methode test ist eine Testroutine fuer Sortieralgorithmen. Sie erzeugt eine
     * csv-Datei, die zur Erstellung eines Diagramms des Laufzeitverhaltens verwendet werden kann.
     * 
     *  
     * @param   int schrittweite Um wie viel soll die Anzahl in jedem Durchgang erh�ht werden
     * @param   int maxElemente Wie gro� soll die maximale Anzahl an zu sortierenden Elementen sein
     */
    public void test(int anzWiederholungen, int schrittweite, int maxElemente)
    {
        sort.setKonsolenausgabe(false);
        int elemente = schrittweite;
        Random r = new Random();
        boolean append = false;
        while(elemente <= maxElemente)
        {
            for (int i=0; i<anzWiederholungen+2 ; i++)
            {
                if(i < anzWiederholungen) sort.feldErzeugenZufaellig(elemente,elemente);
                if(i == anzWiederholungen) sort.feldErzeugenAbsteigend(elemente);
                if(i == anzWiederholungen+1) sort.feldErzeugenAufsteigend(elemente);

                sort.sort();
                datensatz[0]=sort.getName();
                if(i == anzWiederholungen) datensatz[0]+=" (Abst.)";
                if(i == anzWiederholungen+1) datensatz[0]+=" (Aufst.)";
                datensatz[1]=""+(i+1);
                datensatz[2]=""+elemente;
                datensatz[3]=""+sort.getDauer();
                datensatz[4]=""+sort.getAnzahlVergleiche();
                datensatz[5]=""+sort.getAnzahlVertauschungen();
                csv.writeCSV(sort.getName()+".csv", attribute, datensatz, append);
                append = true;
            }

            elemente += schrittweite;
        }
        sort.setKonsolenausgabe(true);
    }
}