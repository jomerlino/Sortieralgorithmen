/*#
 * Funktioniert noch nicht korrekt! Fehlersuche!!!
 */

public class TournamentSort extends SortingAlgorithm
{
    private BinaerBaum b;
    private int aktTiefe;
    private int zeigerSort;

    public TournamentSort()
    {
        this(100);
    }

    public TournamentSort(int anzahl){
        super(anzahl);
        name = "TournamentSort";
        b = new BinaerBaum();
        b.fuelleBlaetterMitArray(getArray());
        aktTiefe = b.getTiefe();
        zeigerSort = 0;
    }

    public void neuesFeld(int anzahlElemente) {
        feldErzeugenZufaellig(anzahlElemente,anzahlElemente);
        b = new BinaerBaum();
        b.fuelleBlaetterMitArray(getArray());
        aktTiefe = b.getTiefe();
        zeigerSort = 0;
    }

    public BinaerBaum getBaum(){
        return b;
    }

    private int wettkampf(int pos){
        if(b.getInhalt(pos)==0 && b.getInhalt(pos+1)==0) return -1;     // beide leer --> -1
        vergleiche++;
        if(b.getInhalt(pos)>0 && b.getInhalt(pos+1)==0){        // rechts leer
            b.schiebeSohnAufVaterPos(pos);
            vertauschungen++;
            return pos;
        }
        if(b.getInhalt(pos)==0 && b.getInhalt(pos+1)>0){        // links leer
            b.schiebeSohnAufVaterPos(pos+1);
            vertauschungen++;
            return pos+1;
        }
        if(b.getInhalt(pos) <= b.getInhalt(pos+1)) {
            b.schiebeSohnAufVaterPos(pos);
            vertauschungen++;
            return pos;
        }
        else {
            b.schiebeSohnAufVaterPos(pos+1);
            vertauschungen++;
            return pos+1;
        }
    }

    private void nachruecken(int pos){
        // schiebe Elemente iterativ von unten nach oben nach, falls frei
        while(pos != -1 && !b.istBlattPos(pos)){     // solange man noch nicht am Blatt angekommen ist
            if(b.getInhalt(pos)==0){      //oben frei
                int i = b.getLinkerSohnPos(pos);    // gehe zum linken Sohn
                pos = wettkampf(i);
            }
        }
        // am Blatt angekommen -- Ende
    }

    private void raus(){
        // Oberstes Element in das Array sortiert schreiben
        if(b.getInhalt(0)>0){
            setWert(zeigerSort, b.getInhalt(0));
            zeigerSort++;
            b.setInhalt(0,0);
        }
    }

    public void sort(){
        start();
        //# Hier kommt dein Sortieralgorithmus hin.
        //# Speichere deine sortierte Liste im Array sortiert:
        int anfang;
        int ende;
        while(!b.isLeer()){   // Baum ist nicht leer
            anfang = (int)Math.pow(2,aktTiefe)-1;
            ende = (int)Math.pow(2,aktTiefe+1)-2;
            // Wettkampf - gehe alle Elemente der aktTiefe durch
            for(int i=anfang; i<ende; i=i+2){       // es wird aufsteigend sortiert
                int pos = wettkampf(i);
                nachruecken(pos);
            }
            raus();
            if(aktTiefe>1) aktTiefe--;
        }

        stop();
    }
}