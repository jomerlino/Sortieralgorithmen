/**
 * @author Dirk Zechnall
 * @version 11.05.2011
 */
public class InsertSort extends SortingAlgorithm
{
    /**
     * Im Konstruktor werden mit der Anzahl der Elemente die Groesse des
     * Feldes festgelegt.
     * @param int anzahlElemente: Die Anzahl der zu sortierenden Zahlen
     */
    public InsertSort()
    {
        super();
        name = "InsertSort";
    }

    /**
     * Methoden - Fähigkeiten von Objekten der Klasse Feld 
     */
    public void sort(){
        start();
        /*#
         * Aufgaben:
         * 1)  Implementiere in der Methode "public void sort()" den InsertionSort-Algorithmus.
         * 2)  Erzeuge mind. zwei Objekte der Klasse und vergleiche in den Inspect-Fenstern
         *     die Felder und die Sortierdauer vor und nach dem Sortieren
         */
        int anfang;
        int zeiger;
        int ende = array.length-1;

        // Variante 1: Galenbacher Kartensortierer
        for (anfang=1; anfang<=ende; anfang++)
        {
            for (zeiger=0; zeiger<anfang; zeiger++)
            {
                if(istInhaltGroesser(zeiger,anfang)) tauscheInhalteAnPos(zeiger,anfang);
            }
        }
        
        // Variante 2: Spielkarten einzeln aufnehmen und einsortieren
        // for (anfang=1; anfang<=ende; anfang++)
        // {
            // for (zeiger=anfang; zeiger>0; zeiger--)
            // {
                // if(istInhaltGroesser(zeiger-1,zeiger)) tauscheInhalteAnPos(zeiger,zeiger-1);
            // }
        // }

        stop();
    }
}