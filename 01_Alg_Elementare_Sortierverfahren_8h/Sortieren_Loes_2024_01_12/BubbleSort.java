/**
 * @author Dirk Zechnall
 * @version 11.05.2011
 */
public class BubbleSort extends SortingAlgorithm
{
    /**
     * Im Konstruktor werden mit der Anzahl der Elemente die Groesse des
     * Feldes festgelegt.
     * @param int anzahlElemente: Die Anzahl der zu sortierenden Zahlen
     */
    public BubbleSort()
    {
        super();
        name = "BubbleSort";
    }

    /**
     * Methoden - Fähigkeiten von Objekten der Klasse Feld 
     */
    public void sort(){
        start();
        /*#
         * Aufgaben:
         * 1)  Implementiere in der Methode "public void sort()" den BubbleSort-Algorithmus.
         * 2)  Erzeuge mind. zwei Objekte der Klasse und vergleiche in den Inspect-Fenstern
         *     die Felder und die Sortierdauer vor und nach dem Sortieren
         */
        boolean unsortiert=true;
        int temp;

        while (unsortiert){
            unsortiert = false;
            for (int i=0; i < getLaenge()-1; i++) 
                if (istInhaltKleiner(i+1, i)) {                      
                    tauscheInhalteAnPos(i, i+1);
                    unsortiert = true;
                }
        }
        stop();
    }
}