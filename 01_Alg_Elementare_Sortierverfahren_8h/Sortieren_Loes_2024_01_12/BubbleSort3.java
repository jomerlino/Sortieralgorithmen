/**
 * @author Dirk Zechnall
 * @version 11.05.2011
 */
public class BubbleSort3 extends SortingAlgorithm
{
    /**
     * Im Konstruktor werden mit der Anzahl der Elemente die Groesse des
     * Feldes festgelegt.
     * @param int anzahlElemente: Die Anzahl der zu sortierenden Zahlen
     */
    public BubbleSort3()
    {
        super();
        name = "BubbleSort3";
    }

    /**
     * Methoden - Fähigkeiten von Objekten der Klasse Feld 
     */
    public void sort(){
        start();
        
        // Implementierung, bei der immer eins weniger weit gegengen wird und, falls keine
        // Vertauschungen vorkommen, abgebrochen wird (da dann schon sortiert!)
        boolean unsortiert=true;
        int temp=0;
        while (unsortiert){
            unsortiert = false;
            for (int i=0; i < getLaenge()-1-temp; i++) {
                if (istInhaltKleiner(i+1, i)) {                      
                    tauscheInhalteAnPos(i, i+1);
                    unsortiert = true;
                }
            }
            temp++;
        }

        stop();
    }
}