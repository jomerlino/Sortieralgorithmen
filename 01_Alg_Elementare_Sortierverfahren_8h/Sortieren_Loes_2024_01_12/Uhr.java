/**
 * Beschreiben Sie hier die Klasse Uhr.
 * 
 * @author Dirk Zechnall
 * @version 11.05.2011
 */
public class Uhr
{
    private long startzeit;
    private long endzeit;
    private boolean laeuft;

    /**
     * Konstruktor f�r Objekte der Klasse Uhr
     */
    public Uhr()
    {
        startzeit = 0;
        endzeit = 0;
        laeuft = false;
    }

    public void start()
    {
        if (!laeuft) startzeit = System.currentTimeMillis();
        laeuft = true;
    }

    public void stop()
    {
        if (laeuft) endzeit = System.currentTimeMillis();
        laeuft = false;
    }

    public long getDauer()
    {
        if (laeuft) return System.currentTimeMillis()-startzeit;
        else return endzeit-startzeit;
    }

    public String toString()
    {
        String s="";
        if (startzeit != 0){
            s += "Startzeit = "+startzeit+"\n";
            if (laeuft) s += "Die Uhr l�uft noch!";
            else {
                s += "Endzeit = "+endzeit+"\n";
                s += "Dauer = "+getDauer();
            }
        }
        return s;
    }

    public void ausgabe()
    {
        System.out.println(toString());
    }
}