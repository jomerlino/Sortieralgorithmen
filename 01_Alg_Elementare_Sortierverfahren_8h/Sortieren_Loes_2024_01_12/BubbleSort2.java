/**
 * @author Dirk Zechnall
 * @version 11.05.2011
 */
public class BubbleSort2 extends SortingAlgorithm
{
    /**
     * Im Konstruktor werden mit der Anzahl der Elemente die Groesse des
     * Feldes festgelegt.
     * @param int anzahlElemente: Die Anzahl der zu sortierenden Zahlen
     */
    public BubbleSort2()
    {
        super();
        name = "BubbleSort2";
    }

    /**
     * Methoden - Fähigkeiten von Objekten der Klasse Feld 
     */
    public void sort(){
        start();

        // Implementierung, bei der Bubble immer eins weniger weit geht
        int temp;
        for (int j=0; j< getLaenge()-1; j++){
            for (int i=0; i < getLaenge()-1-j; i++) 
                if (istInhaltKleiner(i+1, i)) {                      
                    tauscheInhalteAnPos(i, i+1);
                }
        }

        stop();
    }
}