public class Datenverwaltung {

	/**
	 * Array, das die zu sortierenden Daten enthält
	 */
	private int[] daten;

	/**
	 * Variable, in der die Anzahl der Vergleiche gespeichert werden kann.
	 * Inhalt wird automatisch von Oberfläche übernommen.
	 */
	private int anzahlVergleiche = 0;

	/**
	 * Variable, in die die Laufzeit des Algorithmus in Millisekunden
	 * gespeichert werden kann. Wert wird automatisch in die Oberfläche
	 * übernommen.
	 */
	private long laufzeit = 0;

	
	/**
	 * Methode, die die Elemente an den beiden vorgegebenen Stellen vertauscht 
	 * 
	 * @param ersterIndex - Index des ersten Elements
	 * @param zweiterIndex - Indes des zweiten Elements
	 */
	private void tauscheElementeAnPositionen(int ersterIndex, int zweiterIndex) {
		// Element an erster Position in Zwischenspeicher merken
		int zwischenspeicher = daten[ersterIndex]; 
		// Element an zweiter Position an erste Position kopieren 
		daten[ersterIndex] = daten[zweiterIndex];
		// Element aus Zwischenspeicher in zweite Position kopieren
		daten[zweiterIndex] = zwischenspeicher; 
	}
	
	/**
	 * Sortiert die Daten im Array int[] daten mit Hilfe von Selectionsort
	 */
	public void selectionSort() {
		// Für Laufzeitüberprüfung Startzeitpunkt ermitteln.
		long startzeit = System.currentTimeMillis();
		// Anzahl Vergleich auf 0 setzen
		anzahlVergleiche = 0;
		// Implementierung SelectionSort
		int anfang = 0;
		int speicherzeiger = 0;
		while (anfang < daten.length - 1) {
			speicherzeiger = anfang + 1;
			int min = anfang;
			while (speicherzeiger < daten.length-1) {
				// zuerst Vergleichszähler erhöhen.
				anzahlVergleiche = anzahlVergleiche + 1;
				if (daten[speicherzeiger] <= daten[min]) {
					min = speicherzeiger;
				}
				speicherzeiger = speicherzeiger + 1;
			}
			tauscheElementeAnPositionen(anfang, min);			
			anfang = anfang + 1;
		}
		// Laufzeit berechnen
		laufzeit = (System.currentTimeMillis() - startzeit);
	}

	/**
	 * sortiert die Daten im Array int[] daten mit Hilfe von Bubblesort
	 */
	public void bubbleSort() {
		// Für Laufzeitüberprüfung Startzeitpunkt ermitteln.
		long startzeit = System.currentTimeMillis();
		// Anzahl Vergleich auf 0 setzen
		anzahlVergleiche = 0;
		// Implementierung BubbleSort
		int anfang = 0;
		int speicherzeiger = 0;
		int ende = daten.length - 1;
		while (ende > anfang) {
			while (speicherzeiger < ende) {
				// zuerst Vergleichszähler erhöhen.
				anzahlVergleiche = anzahlVergleiche + 1;
				if (daten[speicherzeiger] > daten[speicherzeiger + 1]) {
					tauscheElementeAnPositionen(speicherzeiger, speicherzeiger + 1);
				}
				speicherzeiger = speicherzeiger + 1;
			}
			speicherzeiger = 0;
			ende = ende - 1;
		}
		// Laufzeit berechnen
		laufzeit = (System.currentTimeMillis() - startzeit);
	}

	/**
	 * sortiert die Daten im Array int[] daten mit Hilfe von Insertionsort
	 */
	public void insertionSort() {
		// Für Laufzeitüberprüfung Startzeitpunkt ermitteln.
		long startzeit = System.currentTimeMillis();
		// Anzahl Vergleich auf 0 setzen
		anzahlVergleiche = 0;
		// Implementierung Insertionsort
		int zwischenspeicher;
		int speicherzeiger;
		int anfang = 0;
		while (anfang <= daten.length - 1) {
			// einzufügendes Element in Zwischenspeicher speichern 
			zwischenspeicher = daten[anfang];
			speicherzeiger = anfang; 
			// Alle Elemente die größer als das einzufügende Element sind eins nach rechts verschieben
			while (speicherzeiger > 0 && daten[speicherzeiger-1] > zwischenspeicher) {
				anzahlVergleiche = anzahlVergleiche + 1; 
				daten[speicherzeiger] = daten[speicherzeiger-1];
				speicherzeiger = speicherzeiger - 1; 
			}
			//noch fehlenden Vergleich nachträglich hinzuzählen
			anzahlVergleiche = anzahlVergleiche + 1; 
			// Element einfügen 
			daten[speicherzeiger] = zwischenspeicher; 
			anfang = anfang + 1; 
		}
		// Laufzeit berechnen
		laufzeit = (System.currentTimeMillis() - startzeit);

	}

	/**
	 * sucht den Datensatz mit der übergebenen Nummer </b> Methode gibt die
	 * Position des Datensatzes im Array zurück </b> Rückgabewert -1, falls
	 * Datensatz nicht gefunden
	 * 
	 * @param datensatz
	 *            - zu suchender Datensatz
	 * @return Position des Datensatzes im Array
	 */
	public int sucheDatensatz(int datensatz) {
		int position = -1;
		int anfang = 0;
		int ende = daten.length - 1;
		int speicherzeiger;
		// suchen, solange nicht gefunden und mehr als ein Element übrig
		while (position == -1 && anfang <= ende) {
			// Speicherzeiger auf Mitte der zu untersuchende Liste setzen
			speicherzeiger = (anfang + ende) / 2;
			if (daten[speicherzeiger] == datensatz) {
				// Datensatz gefunden -> Position speichern
				position = speicherzeiger;
			} else if (daten[speicherzeiger] > datensatz) {
				// Datensatz in linker Hälfte -> Ende verschieben
				ende = speicherzeiger - 1;
			} else {
				// Datensatz in rechter Hälfte -> Anfang verschieben
				anfang = speicherzeiger + 1;
			}
		}
		return position;
	}

	/**********************************************************
	 * 
	 * AB HIER NICHTS MEHR ÄNDERN!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	 * 
	 ********************************************************** 
	 */

	/**
	 * stellt das Datensatzarray für die Oberfläche bereit
	 * 
	 * @return Array mit Datensätzen
	 */
	public int[] getDaten() {
		return daten;
	}

	/**
	 * stellt die Anzahl der Vergleiche für die Oberfläche bereit
	 * 
	 * @return Bei Algorithmus ermittelte Anzahl der Vergleiche
	 */
	public int getAnzahlVergleiche() {
		return anzahlVergleiche;
	}

	/**
	 * stellt die Laufzeit des Algorithmus für Oberfläche bereit
	 * 
	 * @return Bei Algorithmus ermittelte Laufzeit
	 */
	public int getLaufzeit() {
		return (int) laufzeit;
	}

	/**
	 * Füllt das Array daten mit Zufallszahlen zwischen 0 und der Zehnfachen
	 * Anzahl
	 * 
	 * @param Anzahl
	 *            der zu erzeugende Datensätze
	 */
	public void zufaelligeDatensaetzeErzeugen(int anzahl) {
		daten = new int[anzahl];
		for (int i = 0; i < anzahl; i++) {
			daten[i] = (int) (Math.random() * anzahl * 10);
		}
	}

	/**
	 * Füllt das Array daten mit Zufallszahlen zwischen 0 und der Hälfte der
	 * Anzahl -> häufige Wiederholungen
	 * 
	 * @param Anzahl
	 *            der zu erzeugende Datensätze
	 */
	public void datensaetzeMitWiederholungErzeugen(int anzahl) {
		daten = new int[anzahl];
		for (int i = 0; i < anzahl; i++) {
			daten[i] = (int) (Math.random() * anzahl / 2);
		}
	}
}
