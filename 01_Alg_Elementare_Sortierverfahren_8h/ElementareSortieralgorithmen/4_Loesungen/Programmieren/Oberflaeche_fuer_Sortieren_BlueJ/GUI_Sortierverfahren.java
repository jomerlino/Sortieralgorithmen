import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JTextArea;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JScrollPane;
import javax.swing.JLabel;

/**
 * @author Matthias Zimmer 
 * @version 1.0
 * Oberfläche zur Wiedergabe von Sortierergebnissen 
 *
 */
public class GUI_Sortierverfahren extends JFrame {

	private JPanel contentPane;
	private JTextField textFieldAnzahlDatensaetze;
	JTextArea textAreaDatensaetze;
	private Datenverwaltung datenverwaltung;
	private JScrollPane scrollPane;
	private JLabel lblSortierdauer;
	private JTextField textFieldZuSuchenderDatensatz;
	private JLabel lblAusgabeDatensatzSuche;
	private JLabel lblAnzahlVergleiche ;

	/**
	 * Create the frame.
	 */
	public GUI_Sortierverfahren() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 466, 669);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		textFieldAnzahlDatensaetze = new JTextField();
		textFieldAnzahlDatensaetze.setText("5");
		textFieldAnzahlDatensaetze.setBounds(10, 11, 131, 23);
		contentPane.add(textFieldAnzahlDatensaetze);
		textFieldAnzahlDatensaetze.setColumns(10);

		JButton btnDatensaetzeErzeugen = new JButton("zuf\u00E4llige Datens\u00E4tze erzeugen");
		btnDatensaetzeErzeugen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				datenverwaltung.zufaelligeDatensaetzeErzeugen(Integer.parseInt(textFieldAnzahlDatensaetze.getText()));
				datensaetzeAusgeben();
			}
		});
		btnDatensaetzeErzeugen.setBounds(151, 10, 290, 23);
		contentPane.add(btnDatensaetzeErzeugen);

		JButton btnSelectionsort = new JButton("Selectionsort durchf\u00FChren");
		btnSelectionsort.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				datenverwaltung.selectionSort();
				datensaetzeAusgeben();
			}
		});
		btnSelectionsort.setBounds(10, 100, 431, 23);
		contentPane.add(btnSelectionsort);

		JButton btnBubbleSort = new JButton("Bubblesort durchf\u00FChren");
		btnBubbleSort.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				datenverwaltung.bubbleSort();
				datensaetzeAusgeben();
			}
		});
		btnBubbleSort.setBounds(10, 134, 431, 23);
		contentPane.add(btnBubbleSort);

		scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 224, 431, 337);
		contentPane.add(scrollPane);

		textAreaDatensaetze = new JTextArea();
		textAreaDatensaetze.setEditable(false);
		scrollPane.setViewportView(textAreaDatensaetze);

		lblSortierdauer = new JLabel("Sortierdauer: 0 s ");
		lblSortierdauer.setBounds(10, 199, 131, 14);
		contentPane.add(lblSortierdauer);

		JButton btnDatensaetzeMitWiederholung = new JButton("Datens\u00E4tze mit Wiederholungen erzeugen");
		btnDatensaetzeMitWiederholung.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				datenverwaltung
						.datensaetzeMitWiederholungErzeugen(Integer.parseInt(textFieldAnzahlDatensaetze.getText()));
				datensaetzeAusgeben();
			}
		});
		btnDatensaetzeMitWiederholung.setBounds(151, 44, 290, 23);
		contentPane.add(btnDatensaetzeMitWiederholung);
		
		textFieldZuSuchenderDatensatz = new JTextField();
		textFieldZuSuchenderDatensatz.setText("0");
		textFieldZuSuchenderDatensatz.setBounds(10, 572, 131, 20);
		contentPane.add(textFieldZuSuchenderDatensatz);
		textFieldZuSuchenderDatensatz.setColumns(10);
		
		JButton btnDatensatzSuchen = new JButton("Datensatz in linkem Feld suchen");
		btnDatensatzSuchen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int datensatz = Integer.parseInt(textFieldZuSuchenderDatensatz.getText());
				int position =datenverwaltung.sucheDatensatz(datensatz);   
				if (position == -1)  {
					lblAusgabeDatensatzSuche.setText("Datensatz nicht enthalten");
				}
				else {
					lblAusgabeDatensatzSuche.setText("Der Datensatz \""+ datensatz+"\" befindet sich an Position "+position );
				}
				
			}
		});
		btnDatensatzSuchen.setBounds(151, 572, 290, 23);
		contentPane.add(btnDatensatzSuchen);
		
		lblAusgabeDatensatzSuche = new JLabel("Button klicken, um Datensatz zu suchen. Suche nur bei sortierter Liste m\u00F6glich!");
		lblAusgabeDatensatzSuche.setBounds(10, 605, 430, 14);
		contentPane.add(lblAusgabeDatensatzSuche);
		
		JButton btnInsertionSort = new JButton("Insertionsort durchf\u00FChren");
		btnInsertionSort.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				datenverwaltung.insertionSort();
				datensaetzeAusgeben();
			}
		});
		btnInsertionSort.setBounds(10, 168, 431, 23);
		contentPane.add(btnInsertionSort);
		
		lblAnzahlVergleiche = new JLabel("Anzahl Vergleiche:  0");
		lblAnzahlVergleiche.setBounds(225, 199, 216, 14);
		contentPane.add(lblAnzahlVergleiche);

		// Instanz der Datenverwaltung erzeugen
		datenverwaltung = new Datenverwaltung();
	}

	// Gibt die Datensätze aus dem Objekt datenverwaltung aus
	private void datensaetzeAusgeben() {
		// Ausgabefeld leeren
		textAreaDatensaetze.setText("");
		// Datensätze ausgeben
		for (int i = 0; i < datenverwaltung.getDaten().length; i++) {
			textAreaDatensaetze.append(datenverwaltung.getDaten()[i] + "\n");
		}
		// Zeitdauer setzen
		lblSortierdauer.setText("Sortierdauer: " + datenverwaltung.getLaufzeit() + " ms");
		
		// Anzahl Vergleiche setzen
		lblAnzahlVergleiche.setText("Anzahl Vergleiche: " + datenverwaltung.getAnzahlVergleiche());
	}
}
