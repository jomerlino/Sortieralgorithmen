import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JTextArea;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JScrollPane;
import javax.swing.JLabel;

/**
 * @author Matthias Zimmer, Uwe Seckinger
 * @version 2017.04.22
 * Oberfläche zur Wiedergabe von Sortierergebnissen 
 *
 */
public class GUI_Sortierverfahren extends JFrame {

    private JPanel contentPane;
    private JTextField textFieldAnzahlDatensaetze;
    JTextArea textAreaDatensaetze;
    private Datenverwaltung datenverwaltung;
    private JScrollPane scrollPane;
    private JLabel lblSortierdauer;
    private JLabel lblAnzahlVergleiche;
    private JLabel lblAnzahlGefDatensaetze;
    private JTextField textFieldZuSuchenderDatensatz;
    private JLabel lblAusgabeDatensatzSuche;

    /**
     * Create the frame.
     */
    public GUI_Sortierverfahren() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 466, 603);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);

        textFieldAnzahlDatensaetze = new JTextField();
        textFieldAnzahlDatensaetze.setText("5");
        textFieldAnzahlDatensaetze.setBounds(10, 11, 131, 21);
        contentPane.add(textFieldAnzahlDatensaetze);
        textFieldAnzahlDatensaetze.setColumns(10);
        lblAusgabeDatensatzSuche = new JLabel();

        JButton btnDatensaetzeErzeugen = new JButton("zuf\u00E4llige Datens\u00E4tze erzeugen");
        btnDatensaetzeErzeugen.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent arg0) {
                    lblAusgabeDatensatzSuche.setText("Button klicken, um sortierten Datensatz zu durchsuchen.");
                    datenverwaltung.zufaelligeDatensaetzeErzeugen(Integer.parseInt(textFieldAnzahlDatensaetze.getText()));
                    datensaetzeAusgeben();
                }
            });
        btnDatensaetzeErzeugen.setBounds(151, 10, 290, 23);
        contentPane.add(btnDatensaetzeErzeugen);

        JButton btnSortieren = new JButton("Sortieren");
        btnSortieren.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    lblAusgabeDatensatzSuche.setText("Button klicken, um sortierten Datensatz zu durchsuchen.");
                    datenverwaltung.sortieren();
                    datensaetzeAusgeben();
                }
            });
        btnSortieren.setBounds(10, 100, 431, 23);
        contentPane.add(btnSortieren);

        scrollPane = new JScrollPane();
        scrollPane.setBounds(10, 156, 431, 337);
        contentPane.add(scrollPane);

        textAreaDatensaetze = new JTextArea();
        textAreaDatensaetze.setEditable(false);
        scrollPane.setViewportView(textAreaDatensaetze);

        lblSortierdauer = new JLabel("Sortierdauer: 0 s ");
        lblSortierdauer.setBounds(10, 131, 200, 14);
        contentPane.add(lblSortierdauer);

        JButton btnDatensaetzeMitWiederholung = new JButton("Datens\u00E4tze mit Wiederholungen erzeugen");
        btnDatensaetzeMitWiederholung.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent arg0) {
                    datenverwaltung.datensaetzeMitWiederholungErzeugen(Integer.parseInt(textFieldAnzahlDatensaetze.getText()));
                    datensaetzeAusgeben();
                }
            });
        btnDatensaetzeMitWiederholung.setBounds(151, 44, 290, 23);
        contentPane.add(btnDatensaetzeMitWiederholung);

        lblAnzahlVergleiche = new JLabel("Anzahl Vergleiche:  0");
        lblAnzahlVergleiche.setBounds(225, 131, 216, 14);
        contentPane.add(lblAnzahlVergleiche);

        textFieldZuSuchenderDatensatz = new JTextField();
        textFieldZuSuchenderDatensatz.setText("0");
        textFieldZuSuchenderDatensatz.setBounds(10, 513, 131, 21);
        contentPane.add(textFieldZuSuchenderDatensatz);
        textFieldZuSuchenderDatensatz.setColumns(10);

        JButton btnDatensatzSuchen = new JButton("Datensatz in linkem Feld suchen");
        btnDatensatzSuchen.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent arg0) {
                    int datensatz = Integer.parseInt(textFieldZuSuchenderDatensatz.getText());
                    int position =datenverwaltung.sucheDatensatz(datensatz);   
                    if (position == -1)  {
                        lblAusgabeDatensatzSuche.setText("Datensatz nicht enthalten");
                    }  else if (position == -2) {
                        lblAusgabeDatensatzSuche.setText("Der Datensatz wurde nicht sortiert ");
                    }
                    else {
                        lblAusgabeDatensatzSuche.setText("Der Datensatz \""+ datensatz+"\" befindet sich an Position "+position );
                        lblAnzahlGefDatensaetze.setText("Anzahl Funde: "+datenverwaltung.getAnzGesDatensaetze());
                    }

                }
            });
        btnDatensatzSuchen.setBounds(151, 512, 290, 23);
        contentPane.add(btnDatensatzSuchen);

        //         lblAusgabeDatensatzSuche = new JLabel("Button klicken, um sortierten Datensatz zu durchsuchen.");
        lblAusgabeDatensatzSuche.setBounds(10, 492, 430, 14);
        contentPane.add(lblAusgabeDatensatzSuche);
        
        lblAnzahlGefDatensaetze = new JLabel("Anzahl Funde:  0");
        lblAnzahlGefDatensaetze.setBounds(10, 537, 216, 14);
        contentPane.add(lblAnzahlGefDatensaetze);

        // Instanz der Datenverwaltung erzeugen
        datenverwaltung = new Datenverwaltung();
        this.setVisible(true);

    }

    // Gibt die Datensätze aus dem Objekt datenverwaltung aus
    private void datensaetzeAusgeben() {
        // Ausgabefeld leeren
        textAreaDatensaetze.setText("");
        // Datensätze ausgeben
        for (int i = 0; i < datenverwaltung.getDaten().length; i++) {
            textAreaDatensaetze.append(datenverwaltung.getDaten()[i] + "\n");
        }
        // Zeitdauer setzen
        lblSortierdauer.setText("Sortierdauer: " + datenverwaltung.getLaufzeit() + " ms");

        // Anzahl Vergleiche setzen
        lblAnzahlVergleiche.setText("Anzahl Vergleiche: " + datenverwaltung.getAnzahlVergleiche());
    }

    public Datenverwaltung getDV(){
        return datenverwaltung;
    }
}
