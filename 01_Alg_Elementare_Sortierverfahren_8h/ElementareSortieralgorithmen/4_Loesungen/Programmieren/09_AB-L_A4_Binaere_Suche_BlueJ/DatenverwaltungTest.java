
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * Die Test-Klasse DatenverwaltungTest.
 *
 * @author  (Ihr Name)
 * @version (eine Versionsnummer oder ein Datum)
 */
public class DatenverwaltungTest   {
    private Datenverwaltung datenver1;

    /**
     * Konstruktor fuer die Test-Klasse DatenverwaltungTest
     */
    public DatenverwaltungTest()   {
    }

    /**
     *  Setzt das Testgerüst fuer den Test.
     *
     * Wird vor jeder Testfall-Methode aufgerufen.
     */
    @Before
    public void setUp()   {
        datenver1 = new Datenverwaltung();
    }

    /**
     * Gibt das Testgerüst wieder frei.
     *
     * Wird nach jeder Testfall-Methode aufgerufen.
     */
    @After
    public void tearDown()   {
    }

    @Test
    public void testSortieren()   {
        testSortieren(5);
        testSortieren(100);
        testSortieren(10000);
        testDurchsuchen(5);
        testDurchsuchen(100);
        testDurchsuchen(10000);
    }

    private void testSortieren(int anzDat)  {
        boolean test=true;
        datenver1.zufaelligeDatensaetzeErzeugen(anzDat);
        datenver1.sortieren();
        for(int i=0;i<datenver1.getDaten().length-1;i++){
            test=(test && (datenver1.getDaten()[i]<=datenver1.getDaten()[i+1]));
        }
        System.out.println(anzDat+" Datensätze erfolgreich sortiert.");
    }

    private void testDurchsuchen(int anzDat)  {
        int pos=-1;
        datenver1.zufaelligeDatensaetzeErzeugen(anzDat);
        datenver1.sortieren();
        pos=datenver1.sucheDatensatz(datenver1.getDaten()[anzDat/3]);
        if (pos>-1 && datenver1.getDaten()[anzDat/3]==datenver1.getDaten()[pos]) 
            System.out.println("An Position "+pos+" gefunden");
        else 
            System.out.println("Fehler im Programm");
        pos=datenver1.sucheDatensatz(datenver1.getDaten()[anzDat-1]);
        if (pos>-1 && datenver1.getDaten()[anzDat-1]==datenver1.getDaten()[pos]) 
            System.out.println("An Position "+pos+" gefunden");
        else 
            System.out.println("Fehler im Programm");
        pos=datenver1.sucheDatensatz(datenver1.getDaten()[0]);
        if (pos>-1 && datenver1.getDaten()[0]==datenver1.getDaten()[pos]) 
            System.out.println("An Position "+pos+" gefunden");
        else 
            System.out.println("Fehler im Programm");
    }
}

