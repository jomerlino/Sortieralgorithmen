/**
 * Beschreiben Sie hier die Klasse ZahlZeichen.
 * 
 * @author Uwe Seckinger 
 * @version 2017.06.15
 */
public class ZahlZeichen   {
    // Instanzvariablen - ersetzen Sie das folgende Beispiel mit Ihren Variablen
    private int zahl;
    private char zeichen;
    /**
     * Konstruktor für Objekte der Klasse ZahlZeichen
     * Füllt das Array daten mit Zufallszahlen zwischen 0 und der Hälfte der
     * Anzahl -> häufige Wiederholungen
     * 
     * @param Anzahl    der zu erzeugende Datensätze
     */
    public ZahlZeichen(int anzahl)   {
        zahl = (int) (Math.random() *anzahl);
        zeichen= (char)(Math.random()* 26+65);
    }

    /**
     * stellt die Laufzeit des Algorithmus für Oberfläche bereit
     * 
     * @return Bei Algorithmus ermittelte Laufzeit
     */
    public int getZahl() {
        return zahl;
    }    

    /**
     * stellt die Laufzeit des Algorithmus für Oberfläche bereit
     * 
     * @return Bei Algorithmus ermittelte Laufzeit
     */
    public char getZeichen() {
        return zeichen;
    }
}
