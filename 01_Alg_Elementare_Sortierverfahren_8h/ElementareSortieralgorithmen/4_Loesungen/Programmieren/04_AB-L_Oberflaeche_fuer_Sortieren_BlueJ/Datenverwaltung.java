public class Datenverwaltung {

    private int[] daten;  //Array, das die zu sortierenden Daten enthält
    private int anzahlVergleiche = 0;  //Variable, speichert die Anzahl der Vergleiche gespeichert werden kann. Inhalt wird automatisch von Oberfläche übernommen.
    // Variable, speichert in die die Laufzeit des Algorithmus in Millisekunden Wert wird automatisch in die Oberfläche übernommen.
    private long laufzeit = 0;

    // Methode, die die Elemente an den Stellen ersterIndex und zweiterIndex vertauscht 
    private void tauscheElementeAnPositionen(int ersterIndex, int zweiterIndex) {
        int zwischenspeicher = daten[ersterIndex]; 		// Element an erster Position in Zwischenspeicher merken
        daten[ersterIndex] = daten[zweiterIndex];		// Element an zweiter Position an erste Position kopieren 
        daten[zweiterIndex] = zwischenspeicher; 		// Element aus Zwischenspeicher in zweite Position kopieren
    }

    /**
     * sortiert die Daten im Array int[] daten mit Hilfe von SelectionSort
     * Verwendet werden While-Schleifen
     */
    public void selectionSort() {
        int speicherzeiger;
        int anfang = 0;
        while (anfang< daten.length - 1) {
            speicherzeiger =anfang+ 1;
            int bestes = anfang;
            while (speicherzeiger< daten.length) {
                if (daten[speicherzeiger] >= daten[bestes]) {
                    bestes = speicherzeiger;
                }
                speicherzeiger ++;
            }
            tauscheElementeAnPositionen(anfang,bestes);
            anfang++;
        }
    }

    /**
     * Sortiert die Daten im Array int[] daten mit Hilfe von SelectionSort
     * Verwendet werden For-Schleifen
     */
    public void selectionSortFor(  ) {
        for (int anfang=0; anfang<daten.length-1; anfang++)        {
            int bestes=anfang;
            for (int speicherzeiger=anfang+1; speicherzeiger<daten.length;speicherzeiger++)   {
                if (daten[speicherzeiger]>=daten[bestes]) {
                    bestes=speicherzeiger;
                }
            }
            tauscheElementeAnPositionen(bestes,anfang);
        }
    }

    /**
     * sortiert die Daten im Array int[] daten mit Hilfe von Selectionsort in die anderer Richtung
     */
    public void selectionSortRueck() {
        long startzeit = System.currentTimeMillis();
        for (int anfang=0; anfang<daten.length-1; anfang++)        {
            int bestes=anfang;
            for (int speicherzeiger=anfang+1; speicherzeiger<daten.length;speicherzeiger++)   {
                if (daten[speicherzeiger]<=daten[bestes]) {
                    bestes=speicherzeiger;
                }
                anzahlVergleiche++;
            }
            tauscheElementeAnPositionen(bestes,anfang);
        }
        laufzeit = (System.currentTimeMillis() - startzeit);
    }
    /**********************************************************
     * 
     * AB HIER NICHTS MEHR ÄNDERN!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
     * 
     ********************************************************** 
     */

    /**
     * stellt das Datensatzarray für die Oberfläche bereit
     * 
     * @return Array mit Datensätzen
     */
    public int[] getDaten() {
        return daten;
    }

    /**
     * stellt die Anzahl der Vergleiche für die Oberfläche bereit
     * 
     * @return Bei Algorithmus ermittelte Anzahl der Vergleiche
     */
    public int getAnzahlVergleiche() {
        return anzahlVergleiche;
    }

    /**
     * stellt die Laufzeit des Algorithmus für Oberfläche bereit
     * 
     * @return Bei Algorithmus ermittelte Laufzeit
     */
    public int getLaufzeit() {
        return (int) laufzeit;
    }

    /**
     * Füllt das Array daten mit Zufallszahlen zwischen 0 und der Zehnfachen
     * Anzahl
     * 
     * @param Anzahl
     *            der zu erzeugende Datensätze
     */
    public void zufaelligeDatensaetzeErzeugen(int anzahl) {
        daten = new int[anzahl];
        for (int i = 0; i < anzahl; i++) {
            daten[i] = (int) (Math.random() * anzahl * 10);
        }
    }

    /**
     * Füllt das Array daten mit Zufallszahlen zwischen 0 und der Hälfte der
     * Anzahl -> häufige Wiederholungen
     * 
     * @param Anzahl
     *            der zu erzeugende Datensätze
     */
    public void datensaetzeMitWiederholungErzeugen(int anzahl) {
        daten = new int[anzahl];
        for (int i = 0; i < anzahl; i++) {
            daten[i] = (int) (Math.random() * anzahl / 2);
        }
    }
}
