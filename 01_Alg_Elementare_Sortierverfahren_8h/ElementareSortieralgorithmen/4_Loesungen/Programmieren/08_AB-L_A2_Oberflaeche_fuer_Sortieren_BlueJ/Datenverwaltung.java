public class Datenverwaltung {

    private ZahlZeichen[] daten;  //Array, das die zu sortierenden Daten enthält
    private long anzahlVergleiche = 0;  //Variable, speichert die Anzahl der Vergleiche gespeichert werden kann. Inhalt wird automatisch von Oberfläche übernommen.
    // Variable, speichert in die die Laufzeit des Algorithmus in Millisekunden Wert wird automatisch in die Oberfläche übernommen.
    private long laufzeit = 0;

    // Methode, die die Elemente an den Stellen ersterIndex und zweiterIndex vertauscht 
    private void tauscheElementeAnPositionen(int ersterIndex, int zweiterIndex) {
        ZahlZeichen zwischenspeicher = daten[ersterIndex]; 		// Element an erster Position in Zwischenspeicher merken
        daten[ersterIndex] = daten[zweiterIndex];		// Element an zweiter Position an erste Position kopieren 
        daten[zweiterIndex] = zwischenspeicher; 		// Element aus Zwischenspeicher in zweite Position kopieren
    }

    /**
     * Sortiert die Daten im Array int[] daten mit Hilfe von SelectionSort
     */
    public void selectionSort(  ) {
        long startzeit = System.currentTimeMillis();		// Für Laufzeitüberprüfung Startzeitpunkt ermitteln.
        anzahlVergleiche = 0;		// Anzahl Vergleich auf 0 setzen
        for (int anfang=0; anfang<daten.length-1; anfang++)        {
            int bestes=anfang;
            for (int speicherzeiger=anfang+1; speicherzeiger<daten.length;speicherzeiger++)   {
                if (daten[speicherzeiger].getZahl()<daten[bestes].getZahl()) {
                    bestes=speicherzeiger;
                }  else if (daten[speicherzeiger].getZahl()==daten[bestes].getZahl()&&
                            daten[speicherzeiger].getZeichen()<=daten[bestes].getZeichen()) {
                    bestes=speicherzeiger;
                }

                anzahlVergleiche++;
            }
            tauscheElementeAnPositionen(bestes,anfang);
        }
        laufzeit = (System.currentTimeMillis() - startzeit);
    }

    /**
     * sortiert die Daten im Array int[] daten mit Hilfe von BubbleSort
     */
    public void bubbleSort() {
        long startzeit = System.currentTimeMillis();		// Für Laufzeitüberprüfung Startzeitpunkt ermitteln.
        anzahlVergleiche = 0;		// Anzahl Vergleich auf 0 setzen
        // Implementierung BubbleSort
        int ende = daten.length - 1;
        for (int anfang = 0;anfang<ende;ende--) {
            for (int speicherzeiger = 0;speicherzeiger < ende;speicherzeiger++) {
                anzahlVergleiche++;                // zuerst Vergleichszähler erhöhen.
                if (daten[speicherzeiger].getZahl()> daten[speicherzeiger + 1].getZahl()) {
                    tauscheElementeAnPositionen(speicherzeiger, speicherzeiger + 1);
                }  else  if (daten[speicherzeiger].getZahl()== daten[speicherzeiger + 1].getZahl()&&
                             daten[speicherzeiger].getZeichen()>daten[speicherzeiger + 1].getZeichen()) {
                    tauscheElementeAnPositionen(speicherzeiger, speicherzeiger + 1);
                }
            }
        }
        laufzeit = (System.currentTimeMillis() - startzeit);       // Laufzeit berechnen
    }

    /**********************************************************
     * 
     * AB HIER NICHTS MEHR ÄNDERN!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
     * 
     ********************************************************** 
     */

    /**
     * stellt das Datensatzarray für die Oberfläche bereit
     * 
     * @return Array mit Datensätzen
     */
    public ZahlZeichen[] getDaten() {
        return daten;
    }

    /**
     * stellt die Anzahl der Vergleiche für die Oberfläche bereit
     * 
     * @return Bei Algorithmus ermittelte Anzahl der Vergleiche
     */
    public long getAnzahlVergleiche() {
        return anzahlVergleiche;
    }

    /**
     * stellt die Laufzeit des Algorithmus für Oberfläche bereit
     * 
     * @return Bei Algorithmus ermittelte Laufzeit
     */
    public int getLaufzeit() {
        return (int) laufzeit;
    }

    /**
     * Füllt das Array daten mit Zufallszahlen zwischen 0 und der Hälfte der
     * Anzahl -> häufige Wiederholungen
     * 
     * @param Anzahl
     *            der zu erzeugende Datensätze
     */
    public void datensaetzeMitWiederholungErzeugen(int anzahl) {
        daten = new ZahlZeichen[anzahl];
        for (int i = 0; i < anzahl; i++) {
            daten[i] = new ZahlZeichen(anzahl/2);
        }
    }
}
