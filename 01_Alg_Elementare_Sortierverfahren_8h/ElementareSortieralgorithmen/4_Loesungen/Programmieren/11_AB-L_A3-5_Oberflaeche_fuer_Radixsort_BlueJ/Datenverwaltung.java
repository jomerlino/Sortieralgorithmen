import java.util.*;
public class Datenverwaltung {

    private int[] daten;  //Array, das die zu sortierenden Daten enthält
    private int maxStellenZahl10;
    private int maxStellenZahl2;
    private boolean istZeichen;

    // Methode, die die Elemente an den Stellen ersterIndex und zweiterIndex vertauscht 
    private void tauscheElementeAnPositionen(int ersterIndex, int zweiterIndex) {
        int zwischenspeicher = daten[ersterIndex]; 		// Element an erster Position in Zwischenspeicher merken
        daten[ersterIndex] = daten[zweiterIndex];		// Element an zweiter Position an erste Position kopieren 
        daten[zweiterIndex] = zwischenspeicher; 		// Element aus Zwischenspeicher in zweite Position kopieren
    }

    /**
     * Sortiert die Daten im Array int[] daten mit Hilfe von RadixSort
     * Verwendet wird das Dezimalsystem
     */
    public void radix10Sort(  ) {
        ArrayList[]   fachPart = new ArrayList[10];            // die 10 Faecher
        for(int i=0;i<10;i++) {
            fachPart[i]=new ArrayList<Integer>(); // eine ArrayList fuer jedes Fach
        }
        int fach;
        for (int i=0; i<maxStellenZahl10; i++) {            // Schleife ueber alle Fächer
            int teiler=(int)(Math.pow(10,i));
            // Partitionierungsphase: teilt die Daten auf die Faecher auf
            for (int j=0; j<daten.length; j++) {
                fach = (daten[j])/teiler%10;  // ermittelt die Fachnummer: 0 bis 9                   
                fachPart[fach].add(daten[j]);   // kopiert j-tes Element ins richtige Fach
            }
            // Sammelphase: kopiert die beiden Faecher wieder zusammen
            int pos=0;
            for (int j=0;j<10;j++)   {
                for(int k=0;k<fachPart[j].size();k++) {
                    daten[pos]=(int)(fachPart[j].get(k));
                    pos++;
                }
                fachPart[j].clear();
            }
        }
    }

    /**
     * sortiert die Daten im Array int[] daten mit Hilfe von SelectionSort
     * Verwendet wird das Bin&auml;rsystem
     */
    public void radix2Sort() {
        ArrayList[]   fachPart = new ArrayList[2];            // die 10 Faecher
        for(int i=0;i<2;i++) {
            fachPart[i]=new ArrayList<Integer>(); // eine ArrayList fuer jedes Fach
        }
        int fach;
        for (int i=0; i<maxStellenZahl2; i++) {            // Schleife ueber alle Fächer
            int teiler=(int)(Math.pow(2,i));
            // Partitionierungsphase: teilt die Daten auf die Faecher auf
            for (int j=0; j<daten.length; j++) {
                fach = (daten[j])/teiler%2;  // ermittelt die Fachnummer: 0 bis 1                   
                fachPart[fach].add(daten[j]);   // kopiert j-tes Element ins richtige Fach
            }
            // Sammelphase: kopiert die beiden Faecher wieder zusammen
            int pos=0;
            for (int j=0;j<2;j++)   {
                for(int k=0;k<fachPart[j].size();k++) {
                    daten[pos]=(int)(fachPart[j].get(k));
                    pos++;
                }
                fachPart[j].clear();
            }
        }    
    }

    /**
     * sortiert die Daten im Array int[] daten mit Hilfe von RadixSort
     * Die Zeichen werden als Zahlen von 0 bis 25 festgehalten und nur bei der Ausgabe in das entsprechende 
     * ASCII-Zeichen umgewandelt.
     */
    public void radixAZSort() {
        ArrayList[]   fachPart = new ArrayList[2];            // die 2 Faecher
        for(int i=0;i<2;i++) {
            fachPart[i]=new ArrayList<Integer>(); // eine ArrayList fuer jedes Fach
        }
        int fach;
        for (int i=0; i<maxStellenZahl2; i++) {            // Schleife ueber alle Fächer
            int teiler=(int)(Math.pow(2,i));
            // Partitionierungsphase: teilt die Daten auf die Faecher auf
            for (int j=0; j<daten.length; j++) {
                fach = (daten[j])/teiler%2;  // ermittelt die Fachnummer: 0 oder 1                   
                fachPart[fach].add(daten[j]);   // kopiert j-tes Element ins richtige Fach
            }
            // Sammelphase: kopiert die beiden Faecher wieder zusammen
            int pos=0;
            for (int j=0;j<2;j++)   {
                for(int k=0;k<fachPart[j].size();k++) {
                    daten[pos]=(int)(fachPart[j].get(k));
                    pos++;
                }
                fachPart[j].clear();
            }
        }
    }

    /**********************************************************
     * 
     * AB HIER NICHTS MEHR ÄNDERN!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
     * 
     ********************************************************** 
     */

    /**
     * stellt das Datensatzarray für die Oberfläche bereit
     * 
     * @return Array mit Datensätzen
     */
    public int[] getDaten() {
        return daten;
    }

    /**
     * stellt die Laufzeit des Algorithmus für Oberfläche bereit
     * 
     * @return Bei Algorithmus ermittelte Laufzeit
     */
    public boolean istZeichen() {
        return istZeichen;
    }

    /**
     * Füllt das Array daten mit Zufallszahlen zwischen 0 und der Zehnfachen
     * Anzahl
     * 
     * @param Anzahl
     *            der zu erzeugende Datensätze
     */
    public void zufaelligeDatensaetzeErzeugen(int anzahl) {
        daten = new int[anzahl];
        istZeichen=false;
        for (int i = 0; i < anzahl; i++) {
            daten[i] = (int) (Math.random() * anzahl * 10);
        }
        maxStellenZahl10=(int)(Math.log10(anzahl)+2);
        maxStellenZahl2=(int)(Math.log(10*anzahl)/Math.log(2))+1;

    }

    /**
     * Füllt das Array daten mit Zufallszahlen zwischen 0 und der Hälfte der
     * Anzahl -> häufige Wiederholungen
     * 
     * @param Anzahl
     *            der zu erzeugende Datensätze
     */
    public void datensaetzeMitWiederholungErzeugen(int anzahl) {
        daten = new int[anzahl];
        istZeichen=false;
        for (int i = 0; i < anzahl; i++) {
            daten[i] = (int) (Math.random() * anzahl / 2);
        }
        maxStellenZahl10=(int)(Math.log10(anzahl)+1);
        maxStellenZahl2=(int)(Math.log(anzahl)/Math.log(2));
    }

    public void datensaetzeZeichenErzeugen(int anzahl) {
        daten = new int[anzahl];
        istZeichen=true;
        for (int i = 0; i < anzahl; i++) {
            daten[i] = (int) (Math.random() * 26 );
        }
        maxStellenZahl10=2;
        maxStellenZahl2=5;
    }
}
