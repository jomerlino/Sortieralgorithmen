import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;

class ZeichenThread extends Thread {

  // Anfang Attribute
  private Graphics2D g;
  private int[] feld;
  private int tempo;
  private boolean tPause;
  private int verfahren; // 0: Minsort, 1: Quicksort
  // Ende Attribute


  public ZeichenThread(Graphics2D g, int[] feld) {
    this.g = g;
    this.feld = feld;
    verfahren = 0;
  }

  // Anfang Methoden
  public void warte(int zeit) {
    try {
      Thread.sleep(zeit);
    }
    catch (InterruptedException e) {}

    synchronized (this) { // Play-Pause-Funktion des Threads
      while (tPause) {
        try {
          wait();
        }
        catch (Exception e) {}
      }
    }
  }
  
  public void pause() { // Thread pausieren
    tPause = true;
  }
  
  public void proceed() { // Thread fortsetzen
    tPause = false;
    notify();
  }
  
  public boolean istAktiv() {
    return !tPause;
  }
  
  public void setzeTempo(int tempo) {
    this.tempo = tempo;
  }
  
  public void setzeVerfahren(int verfahren) {
    this.verfahren = verfahren;
  }
  
  private void tausche(int p1, int p2) {
    int hilf = feld[p1];
    feld[p1] = feld[p2];
    feld[p2] = hilf;
  }

  public void zeichneFeld() {
    for (int i=0; i<feld.length; i++) {
      zeichneBalken(i, 300, Color.white); // f�r Neustart: mit wei� �bermalen
      zeichneTrenner(i, 300, Color.white);
      zeichneBalken(i, feld[i], Color.black);
    }
  }

  private void minSort() {
    int minPos;

    for (int i=0; i<feld.length; i++) {
      minPos = i; // Minimum: erste Position im unsortierten Feld
      zeichneBalken(minPos, feld[minPos], Color.blue);
      for (int j=i+1; j<feld.length; j++) {
        zeichneBalken(j, feld[j], Color.red);
        warte(tempo);
        zeichneBalken(j, feld[j], Color.black);
        if (feld[minPos] > feld[j]) { // kleineres Element gefunden
          zeichneBalken(minPos, feld[minPos], Color.black);
          minPos = j; // Position merken
          zeichneBalken(minPos, feld[minPos], Color.blue);
        }
      }
      tausche(i, minPos); // erstes mit kleinstem vertauschen

      zeichneBalken(minPos, 300, Color.white);
      zeichneBalken(minPos, feld[minPos], Color.black);
      zeichneBalken(i, 300, Color.white);
      zeichneBalken(i, feld[i], Color.green);
    }
  }
  
  
  public void bubbleSort() {
    for (int zeiger=0; zeiger<feld.length; zeiger++) {
      for (int i=0; i<feld.length-zeiger-1; i++) {
      
        zeichneBalken(i, feld[i], Color.red);
        zeichneBalken(i+1, feld[i+1], Color.blue);
        warte(tempo);
        
        if (feld[i] > feld[i+1]) {

          tausche(i, i+1);
          zeichneBalken(i+1, 300, Color.white);
          zeichneBalken(i+1, feld[i+1], Color.red);
          zeichneBalken(i, 300, Color.white);
          zeichneBalken(i, feld[i], Color.blue);
          warte(tempo);
        }
        
        zeichneBalken(i, feld[i], Color.black);
        zeichneBalken(i+1, feld[i+1], Color.black);
      }
      zeichneBalken(feld.length-zeiger-1, feld[feld.length-zeiger-1], Color.green);
    }
  }
  
  
  private void quickSort(int l, int r) {

    int i, j, trennwert;
    int letzterI, letzterJ;

    i = l;
    j = r;
    
    // Trenner zeichnen
    zeichneTrenner(l-1, 320, Color.green);
    zeichneTrenner(r, 320, Color.green);
    
    trennwert = feld[(l+r)/2]; // Wert genau aus der Mitte

    while (i<=j) {
      
      while (feld[i] < trennwert) // kleine passende Werte �bergehen
        i++;
      
      while (trennwert < feld[j]) // gro�e passende Werte �bergehen
        j--;

      // kleinen & gr��eren Tauschpartner markieren
      zeichneBalken(i, feld[i], Color.red);
      zeichneBalken(j, feld[j], Color.blue);
      warte(tempo);
      zeichneBalken(i, feld[i], Color.black);
      zeichneBalken(j, feld[j], Color.black);

      if  (i <= j) {

        tausche(i, j);
        zeichneBalken(i, 300, Color.white);
        zeichneBalken(i, feld[i], Color.black);
        zeichneBalken(j, 300, Color.white);
        zeichneBalken(j, feld[j], Color.black);
        warte(tempo);
      
        i++;
        j--;
      }
    }

    // Trenner l�schen
    zeichneTrenner(l-1, 320, Color.white);
    zeichneTrenner(r, 320, Color.white);
    warte(tempo);

    if (l < j)
      quickSort (l, j);
    if (i < r)
      quickSort (i, r);
  }
  
  
  private void zeichneBalken(int pos, int hoehe, Color farbe) {
    g.setColor(farbe);
    g.fillRect(19+pos*7, 39, 5, hoehe);
  }
  
  private void zeichneTrenner(int pos, int hoehe, Color farbe) {
    g.setColor(farbe);
    g.fillRect(24+pos*7, 35, 2, hoehe);
  }
  
  public void run() {
    if (verfahren == 0)
      minSort();
    if (verfahren == 1)
      bubbleSort();
    if (verfahren == 2)
      quickSort(0, feld.length-1);
      
  }
  // Ende Methoden
}