import java.io.*;
/**
 * Auswerten misst die Laufzeit verschiedener Sortierverfahren, indem die Anzahl der Vergleiche gez&auml;hlt werden.
 * Es wird eine Reihung laufzeit erzeugt, deren Indexvariable der Gr&ouml;&szlig;e des zu sortierenden Arrays
 * entspricht und deren Reihenwert die Anzahl der Vergleiche angibt;
 * Das Z&auml;hlen &uuml;bernimmt die Klasse Sort.
 * 
 * Erstelle zuerst ein neues Objekt von Auswerten. Durch den Aufruf von start werden alle Sortierungen durchgef&uuml;hrt.
 * Anschlie&szlig;end wird das Ergebnis auf der Konsole ausgegeben.
 * 
 * @author Uwe Seckinger 
 * @version L&ouml;sung 2017.04.18
 */
public class Auswerten  {

    private Sort s; //Testklasse
    private int anzAus; //Maximale L&auml;nge, die getestet wird;
    private int[] laufzeit; //Reihung, die die Laufzeiten enth&auml;lt

    /**
     * Konstruktor f&uuml;r Objekte der Klasse Auswerten.
     * @param anzAus Ausgewertet werden Reihungen der L&auml;nge 1 bis anzAus     */
    public Auswerten(int anzAus)   {
        this.anzAus=anzAus;
        laufzeit=new int[anzAus];
    }

    /**
     * Startet den Prozess und erstellt f&uuml;r jeden Wert von 1..anzAus ein Objekt der Klasse Sort.
     * Die Laufzeit der Sortierung wird in der Reihung laufzeit eingetragen 
     * 
     * @param sortierVerfahren w&auml;hlt das Verfahren aus, nach dem sortiert wird. 
     * "ins" steht f&uuml;r InsertionSort
     * "sel" steht f&uuml;r SelectionSort
     * "rad" steht f&uuml;r RadixSort */

    public void start(String sortierVerfahren)   {
        for (int i=0; i<anzAus;i++)  {
            s=new Sort(i+1);  
            if (sortierVerfahren.equals("ins"))   {
                laufzeit[i]=s.insSort();
            }
            if (sortierVerfahren.equals("sel"))   {
                laufzeit[i]=s.selSort();
            }
            if (sortierVerfahren.equals("rad"))   {
                laufzeit[i]=s.radSort();
            } 
        }
        for (int i=0; i< laufzeit.length; i++) {
            System.out.println((i+1)+" :  "+laufzeit[i]); }
    }
}
