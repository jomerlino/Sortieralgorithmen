public class Datenverwaltung {

    private int[] daten;  //Array, das die zu sortierenden Daten enthält
    private long anzahlVergleiche = 0;  //Variable, speichert die Anzahl der Vergleiche gespeichert werden kann. Inhalt wird automatisch von Oberfläche übernommen.
    // Variable, speichert in die die Laufzeit des Algorithmus in Millisekunden Wert wird automatisch in die Oberfläche übernommen.
    private long laufzeit = 0;

    // Methode, die die Elemente an den Stellen ersterIndex und zweiterIndex vertauscht 
    private void tauscheElementeAnPositionen(int ersterIndex, int zweiterIndex) {
        int zwischenspeicher = daten[ersterIndex]; 		// Element an erster Position in Zwischenspeicher merken
        daten[ersterIndex] = daten[zweiterIndex];		// Element an zweiter Position an erste Position kopieren 
        daten[zweiterIndex] = zwischenspeicher; 		// Element aus Zwischenspeicher in zweite Position kopieren
    }

    /**
     * Sortiert die Daten im Array int[] daten mit Hilfe von ?Sort
     */
    public void sortieren() {
        long startzeit = System.currentTimeMillis();		// Für Laufzeitüberprüfung Startzeitpunkt ermitteln.
        anzahlVergleiche = 0;		// Anzahl Vergleich auf 0 setzen
        sortieren(0,getDaten().length-1);			//rekursiver Aufruf für rechte Teilfolge
        laufzeit = (System.currentTimeMillis() - startzeit);
    }
    
    public void sortieren(int l,int r) {
        if(r>l){					//solange mehr als 1 Folgenelement existiert
            int i=l-1, j=r;			//Variableninitialisierung mit Folgenrändern
            while(i<j)   {
                while(daten[++i]<daten[r]) anzahlVergleiche++;			//inkrem., bis größeres  Element gefunden wird
                while(daten[--j]>daten[r] && j>i) anzahlVergleiche++;		//dekrem., bis kleineres Element gefunden wird
                if(i<j) tauscheElementeAnPositionen(i,j);
            }
            tauscheElementeAnPositionen(i,r);		//tausche Trennelement
            sortieren(l, i-1);			//rekursiver Aufruf für linke Teilfolge
            sortieren(i+1, r);			//rekursiver Aufruf für rechte Teilfolge
        }
    }

    public int sucheDatensatz(int datensatz) {
        int position = -1;  // Kopiere deinen Suchalgorithmus aus Aufgabe 1 an diese Stelle
        return position;
    }
/*#
 * Zusatz: Setze position =-2 falls die Suche bei unsortiertem Datensatz aufgerufen wird
 */
    /**********************************************************
     * 
     * AB HIER NICHTS MEHR ÄNDERN!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
     * 
     ********************************************************** 
     */

    /**
     * stellt das Datensatzarray für die Oberfläche bereit
     * 
     * @return Array mit Datensätzen
     */
    public int[] getDaten() {
        return daten;
    }

    /**
     * stellt die Anzahl der Vergleiche für die Oberfläche bereit
     * 
     * @return Bei Algorithmus ermittelte Anzahl der Vergleiche
     */
    public long getAnzahlVergleiche() {
        return anzahlVergleiche;
    }

    /**
     * stellt die Laufzeit des Algorithmus für Oberfläche bereit
     * 
     * @return Bei Algorithmus ermittelte Laufzeit
     */
    public int getLaufzeit() {
        return (int) laufzeit;
    }

    /**
     * Füllt das Array daten mit Zufallszahlen zwischen 0 und der Zehnfachen
     * Anzahl
     * 
     * @param Anzahl
     *            der zu erzeugende Datensätze
     */
    public void zufaelligeDatensaetzeErzeugen(int anzahl) {
        anzahlVergleiche = 0;
        daten = new int[anzahl];
        for (int i = 0; i < anzahl; i++) {
            daten[i] = (int) (Math.random() * anzahl * 10);
        }
    }

    /**
     * Füllt das Array daten mit Zufallszahlen zwischen 0 und der Hälfte der
     * Anzahl -> häufige Wiederholungen
     * 
     * @param Anzahl
     *            der zu erzeugende Datensätze
     */
    public void datensaetzeMitWiederholungErzeugen(int anzahl) {
        daten = new int[anzahl];
        for (int i = 0; i < anzahl; i++) {
            daten[i] = (int) (Math.random() * anzahl / 2);
        }
    }
}
