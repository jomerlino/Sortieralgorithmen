
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * Die Test-Klasse DatenverwaltungTest.
 *
 * @author  (Ihr Name)
 * @version (eine Versionsnummer oder ein Datum)
 */
public class DatenverwaltungTest   {
    private Datenverwaltung datenver1;

    /**
     * Konstruktor fuer die Test-Klasse DatenverwaltungTest
     */
    public DatenverwaltungTest()   {
    }

    /**
     *  Setzt das Testgerüst fuer den Test.
     *
     * Wird vor jeder Testfall-Methode aufgerufen.
     */
    @Before
    public void setUp()   {
        datenver1 = new Datenverwaltung();
    }

    /**
     * Gibt das Testgerüst wieder frei.
     *
     * Wird nach jeder Testfall-Methode aufgerufen.
     */
    @After
    public void tearDown()   {
    }

    @Test
    public void testSortieren()   {
        testSortieren(5);
        testSortieren(100);
        testSortieren(10000);
        testDurchsuchen(5);
        testDurchsuchen(100);
        testDurchsuchen(10000);
    }

    private void testSortieren(int anzDat)  {
        boolean test=true;
        datenver1.zufaelligeDatensaetzeErzeugen(anzDat);
        datenver1.sortieren();
/*#  Implementiere den fehlenden Quelltext der prüft, ob die Datensätze sortiert sind
   und das Ergebnis ausgibt*/
        System.out.println(anzDat+" Datensätze erfolgreich sortiert.");
    }

    private void testDurchsuchen(int anzDat)  {
        int pos=-1;
/*#  Implementiere den fehlenden Quelltext der prüft, ob das Element an der Stelle 0, anzDat/3 und an 
 * letzten Stelle gefunden werden und das Ergebnis entsprechend ausgibt */
        if (pos>-1 && true) //true muss ersetzt werden
            System.out.println("An Position "+pos+" gefunden");
        else 
            System.out.println("Fehler im Programm");
    }
}

