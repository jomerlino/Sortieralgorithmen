import java.util.*;
/**
 * Die Klasse Sort sortiert die Werte einer Reihung der Gr&ouml;&szlig;e nach.
 * Zus&auml;tzlich die Laufzeit gemessen und das Ergebnis in die Reihung laufzeit
 * eingetragen. 
 * 
 * @author Uwe Seckinger 
 * @version L&ouml;esung 2016.12.05
 */

public class Sort {

    private int[] reihung; //Zu sortierende Reihung

    /**
     * Konstruktor für Objekte der Klasse SelSort.
     * Erzeugt wird eine Reihung der L&auml;nge laenge, die double Werten enthält.
     */
    public Sort(int laenge)   {
        reihung=new int[laenge];
        for (int i=0;i<reihung.length;i++) reihung[i]=(int)(10000000*Math.random()+1);
    }

    /**
     * selSort sortiert die oben erstellte Reihung (aufsteigend) der Gr&ouml;&szlig;e nach.
     * Verwendet wir Selectionsort.
     * 
     * @return      die Anzahl der Vergleiche     */
    public int selSort()    {
        int zaehler=0;
        int tauschen=0;
        for (int i=0; i<reihung.length-1; i++)        {
            int minpos=i;
            for (int j=i+1; j<reihung.length; j++)   {
                zaehler++;
                if (reihung[j]<reihung[minpos]) 
                    minpos=j;
            }
            tausche(minpos, i);
            tauschen++;
        }
        return zaehler;
    }

    /**
     * insSort sortiert die oben erstellte Reihung (aufsteigend) der Gr&ouml;&szlig;e nach.
     * Verwendet wird Insertionsort.
     * 
     * @return      die Anzahl der Vergleiche     */
    public int insSort()   {
        int zaehler=0;
        int einzusortierenderWert;
        for (int i=1;i<reihung.length;i++) {
            zaehler++;
            einzusortierenderWert =reihung[i];
            int j=i;
            while (j>0 && einzusortierenderWert<reihung[j-1])   {
                reihung[j]=reihung[j-1];
                j--;
                zaehler++;
            }
            reihung[j]=einzusortierenderWert;
        }
        return zaehler;
    }

    public int radSort() {
        int zaehler=0;
        //da nur positive Integerzahlen sortiert werden können, wird eine neue Reihung wird erstellt
        ArrayList[]   fachPart = new ArrayList[10];            // die 10 Faecher
        for(int i=0;i<10;i++) {
            fachPart[i]=new ArrayList<Integer>(); // eine ArrayList fuer jedes Fach
        }
        int fach;
        for (int i=0; i<7; i++) {            // Schleife ueber alle Fächer
            int teiler=(int)(Math.pow(10,i));
            // Partitionierungsphase: teilt "a" auf die Faecher auf
            for (int j=0; j<reihung.length; j++) {
                zaehler++;
                fach = (reihung[j])/teiler%10;  // ermittelt die Fachnummer: 0 oder 1                   
                fachPart[fach].add((Integer)(reihung[j]));   // kopiert j-tes Element ins richtige Fach
            }
            // Sammelphase: kopiert die beiden Faecher wieder nach "a" zusammen
            int pos=0;
            for (int j=0;j<10;j++)   {
                for(int k=0;k<fachPart[j].size();k++) {
                    zaehler++;
                    reihung[pos]=(int)(fachPart[j].get(k));
                    pos++;
                }
                fachPart[j].clear();
            }
        }
        return zaehler;
    }

    // vertauscht zwei Einträge im Array
    private void tausche(int i, int j)    {
        int temp=reihung[i];
        reihung[i]=reihung[j];
        reihung[j]=temp;
    }
}
