public class Datenverwaltung {

    /**
     * Array, das die zu sortierenden Daten enthält
     */
    private int[] daten;

    /**
     * Variable, in der die Anzahl der Vergleiche gespeichert werden kann.
     * Inhalt wird automatisch von Oberfläche übernommen.
     */
    private int anzahlVergleiche = 0;

    /**
     * Variable, in die die Laufzeit des Algorithmus in Millisekunden
     * gespeichert werden kann Wert wird automatisch in die Oberfläche
     * übernommen.
     */
    private long laufzeit = 0;

    /**
	 * Methode, die die Elemente an den beiden vorgegebenen Stellen vertauscht 
	 * 
	 * @param ersterIndex - Index des ersten Elements
	 * @param zweiterIndex - Indes des zweiten Elements
	 */
	private void tauscheElementeAnPositionen(int ersterIndex, int zweiterIndex) {
		// Element an erster Position in Zwischenspeicher merken
		int zwischenspeicher = daten[ersterIndex]; 
		// Element an zweiter Position an erste Position kopieren 
		daten[ersterIndex] = daten[zweiterIndex];
		// Element aus Zwischenspeicher in zweite Position kopieren
		daten[zweiterIndex] = zwischenspeicher; 
	}
    
    
    /**
     * Sortiert die Daten im Array int[] daten mit Hilfe von SelectionSort
     */
    public void selectionSort(  ) {
        // hier Quelltext für Selectionsort einfügen
    }

    /**
     * sortiert die Daten im Array int[] daten mit Hilfe von BubbleSort
     */
    public void selectionSortFor() {
       // hier an der im Unterricht vorgesehenen Stelle Quelltext für Bubblesort einfügen
    }
    
    
    /**
     * sortiert die Daten im Array int[] daten mit Hilfe von Insertionsort
     */
    public void selectionSortRueck() {

        // hier an der im Unterricht vorgesehenen Stelle Quelltext für Insertionsort einfügen

    }
    
    /**********************************************************
     * 
     * AB HIER NICHTS MEHR ÄNDERN!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
     * 
     ********************************************************** 
     */

    /**
     * stellt das Datensatzarray für die Oberfläche bereit
     * 
     * @return Array mit Datensätzen
     */
    public int[] getDaten() {
        return daten;
    }

    /**
     * stellt die Anzahl der Vergleiche für die Oberfläche bereit
     * 
     * @return Bei Algorithmus ermittelte Anzahl der Vergleiche
     */
    public int getAnzahlVergleiche() {
        return anzahlVergleiche;
    }

    /**
     * stellt die Laufzeit des Algorithmus für Oberfläche bereit
     * 
     * @return Bei Algorithmus ermittelte Laufzeit
     */
    public int getLaufzeit() {
        return (int) laufzeit;
    }

    /**
     * Füllt das Array daten mit Zufallszahlen zwischen 0 und der Zehnfachen
     * Anzahl
     * 
     * @param Anzahl
     *            der zu erzeugende Datensätze
     */
    public void zufaelligeDatensaetzeErzeugen(int anzahl) {
        daten = new int[anzahl];
        for (int i = 0; i < anzahl; i++) {
            daten[i] = (int) (Math.random() * anzahl * 10);
        }
    }

    /**
     * Füllt das Array daten mit Zufallszahlen zwischen 0 und der Hälfte der
     * Anzahl -> häufige Wiederholungen
     * 
     * @param Anzahl
     *            der zu erzeugende Datensätze
     */
    public void datensaetzeMitWiederholungErzeugen(int anzahl) {
        daten = new int[anzahl];
        for (int i = 0; i < anzahl; i++) {
            daten[i] = (int) (Math.random() * anzahl / 2);
        }
    }
}
