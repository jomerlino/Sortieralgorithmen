public class BinaerBaum
{
    private int[] baum;
    private int tiefe;
    
    private int[] zahlen; // Das Zahlenarray mit dem der Baum gefuellt wird

    public BinaerBaum(){
        baum = null;
        tiefe = 0;
    }

    private void setzeGroesseAuf(int anzahl){
        baum = new int[anzahl];
        for(int i=0; i<anzahl; i++){
            baum[i]=0;
        }
    }

    public void fuelleBlaetterMitArray(int[] zahlen){
        this.zahlen = zahlen;   // nur damit das Zahlenarray fuer die Textausgabe gespeichert bleibt
        int n = zahlen.length;
        //# Aufgabe 1: Im folgenden soll berechnet werden, wie viele Knoten benoetigt werden,
        //# wenn die Zahlen an die Blaetter geschrieben werden.
        //# Hier gibts also was zu tun - nutze die Math-Bibliothek:
        tiefe = 0;          //# Berechne die Tiefe des Binaerbaums
        int anzKnoten = 0;  //# Berechne die Gesamtanzahl an Knoten im vollstaendigen Binaerbaum
        setzeGroesseAuf(anzKnoten);
        for(int i=0; i<n; i++){
            //# Fuelle die Blaetter mit den gegebenen Zahlen
            
        }
    }

    /**
     * Gibt -1, falls kein Vater d.h. WurzelPos als SohnPos uebergeben
     */
    public int getVaterPos(int sohnPos){
        //# Aufgabe 2: Berechne die Vaterposition
        
        return 0;
    }

    /**
     * Gibt -1, falls kein Vater d.h. WurzelPos als SohnPos uebergeben
     */
    public int getInhaltAnVaterPos(int sohnPos){
        if(sohnPos <= 0) return -1;
        //# Aufgabe 3: Bestimme den Inhalt an der Vaterposition
        
        return 0;
    }

    /**
     * Setzt den SohnInhalt nach dem verschieben auf 0 (bedeutet leer)
     */
    public boolean schiebeSohnAufVaterPos(int sohnPos){
        if(baum[sohnPos]==0 || getInhaltAnVaterPos(sohnPos)==-1 || getInhaltAnVaterPos(sohnPos)!=0) return false;
        else {
            //# Aufgabe 4: Schreibe den Inhalt der Sohnposition an die Vaterposition
            //# und setze anschließend den Sohninhalt auf 0
            
            return true;
        }
    }
    
    public int getTiefe(){
        return tiefe;
    }
    
    public int[] getBaum(){
        return baum;
    }

    public void ausgabe(){
        String s="Binaerbaum aus Array "+java.util.Arrays.toString(zahlen)+":\n";
        if(baum.length>0)s+=baum[0]+"\n";
        for(int i=1; i<baum.length; i++){
            s+=baum[i]+"\t";
            double d = (Math.log(i+2)/Math.log(2));
            int n = (int)d;
            if (d>0 && (d - n) == 0.0) {
                s+="\n";
            }
        }
        s+="\n";
        System.out.println(s);
    }
}