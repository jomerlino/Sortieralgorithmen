/**
 * @author Dirk Zechnall
 * @version 11.05.2011
 */
public class FeldStabilitaet extends Feld
{

    /**
     * Objektvariablen 
     */
 
    private char[] carray;


    /**
     * Konstruktoren - Startzustände von Objekten der Klasse CopyOfFeld
     */
    public FeldStabilitaet(int anzahl, int bereich){
        super(anzahl, bereich);
    }
    
    public FeldStabilitaet() {
        super();
    }


    /**
     * Methoden - Fähigkeiten von Objekten der Klasse CopyOfFeld 
     */
    public void feldfuellen (int anzahl, int bereich){
        super.feldfuellen(anzahl,bereich);
        carray = new char[anzahl];
        for (int i=0; i<carray.length; i++){
            carray[i]=(char)((int)(26*Math.random())+65);
        }
    }

    public void feldKonsolenAusgabe () {
        System.out.print("Index :  ");
        for (int i=0; i<array.length; i++) {
            System.out.print(String.format("%5d  ",i ));
        }
        System.out.println("");
        System.out.print("Inhalt:  ");
        for (int i=0; i<array.length; i++) {
            System.out.print(String.format("%5d%c ",array[i],carray[i]));
        }
        System.out.println("");
    }

    public void tausche(int index1, int index2){
        super.tausche(index1,index2);
        char ctemp = carray[index1];
        carray[index1] = carray[index2];
        carray[index2] = ctemp;
    }

}
