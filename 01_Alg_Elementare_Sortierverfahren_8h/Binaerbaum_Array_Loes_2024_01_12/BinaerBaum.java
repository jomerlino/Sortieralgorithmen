public class BinaerBaum
{
    private int[] baum;
    private int[] zahlen;

    public BinaerBaum(){
        baum = null;
    }

    private void setzeGroesseAuf(int anzahl){
        baum = new int[anzahl];
        for(int i=0; i<anzahl; i++){
            baum[i]=0;
        }
    }

    public void fuelleBlaetterMitArray(int[] zahlen){
        this.zahlen = zahlen;
        int n = zahlen.length;
        int tiefe = (int)Math.ceil((Math.log(n)/Math.log(2)));
        int baumAnzahl = (int)Math.pow(2,tiefe+1)-1;
        setzeGroesseAuf(baumAnzahl);
        for(int i=0; i<n; i++){
            baum[(int)Math.pow(2,tiefe)-1+i] = zahlen[i];
        }
    }

    /**
     * Gibt -1, falls kein Vater d.h. WurzelPos als SohnPos uebergeben
     */
    public int getVaterPos(int sohnPos){
        if(sohnPos <= 0) return -1;
        return (int)((sohnPos-1)/2);
    }

    /**
     * Gibt -1, falls kein Vater d.h. WurzelPos als SohnPos uebergeben
     */
    public int getInhaltAnVaterPos(int sohnPos){
        if(sohnPos <= 0) return -1;
        return baum[getVaterPos(sohnPos)];
    }

    /**
     * Setzt den SohnInhalt nach dem verschieben auf 0 (bedeutet leer)
     */
    public boolean schiebeSohnAufVaterPos(int sohnPos){
        if(baum[sohnPos]==0 || getInhaltAnVaterPos(sohnPos)==-1 || getInhaltAnVaterPos(sohnPos)!=0) return false;
        else {
            baum[getVaterPos(sohnPos)] = baum[sohnPos];
            baum[sohnPos]=0;
            return true;
        }
    }

    public int[] getBaum(){
        return baum;
    }

    public void ausgabe(){
        String s="Binaerbaum aus Array "+java.util.Arrays.toString(zahlen)+":\n";
        if(baum.length>0)s+=baum[0]+"\n";
        for(int i=1; i<baum.length; i++){
            s+=baum[i]+"\t";
            double d = (Math.log(i+2)/Math.log(2));
            int n = (int)d;
            if (d>0 && (d - n) == 0.0) {
                s+="\n";
            }
        }
        s+="\n";
        System.out.println(s);
    }
}