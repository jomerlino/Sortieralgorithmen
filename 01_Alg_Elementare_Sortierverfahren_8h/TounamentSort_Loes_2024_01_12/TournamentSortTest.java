import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

/**
 * Die Test-Klasse TournamentSortTest.
 *
 * @author  (Ihr Name)
 * @version (eine Versionsnummer oder ein Datum)
 */
public class TournamentSortTest
{
    private int[] intArray;
    private TournamentSort tournament;
    private final int MAX = 1000000;

    /**
     * Konstruktor fuer die Test-Klasse TournamentSortTest
     */
    public TournamentSortTest()
    {
    }

    /**
     *  Setzt das Testgerüst fuer den Test.
     *
     * Wird vor jeder Testfall-Methode aufgerufen.
     */
    @BeforeEach
    public void setUp()
    {
        int[] intArray = new int[MAX];
        int oberGrenze = MAX;
        for(int i = 0; i<MAX; i++) {
            intArray[i] = (int)(Math.random()*oberGrenze);
        }
        tournament = new TournamentSort(intArray);
        tournament.ausgabe();
    }

    /**
     * Gibt das Testgerüst wieder frei.
     *
     * Wird nach jeder Testfall-Methode aufgerufen.
     */
    @AfterEach
    public void tearDown()
    {
    }

    @Test
    public void testeSort()
    {
        tournament.sort();
        int[] unsortiert = tournament.getUnsortiert();
        int[] sortiertTS = tournament.getSortiert();
        Arrays.sort(unsortiert);
        assertTrue(Arrays.equals(sortiertTS, unsortiert));
        System.out.println("Der Sortieralgorithmus ist korrekt!");
    }
}