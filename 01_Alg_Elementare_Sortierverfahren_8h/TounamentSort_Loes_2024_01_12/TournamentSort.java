public class TournamentSort extends SortingAlgorithm
{
    private BinaerBaum b;
    private int[] unsortiert;
    private int[] sortiert;
    private int zeigerSort;
    private int aktTiefe;

    public TournamentSort(){
        this(new int[]{4,3,7,2,8});
    }

    public TournamentSort(int[] neueZahlen){
        b = new BinaerBaum();
        unsortiert = neueZahlen;
        b.fuelleBlaetterMitArray(neueZahlen);
        init();
    }

    private void init(){
        sortiert = new int[b.getZahlen().length];
        aktTiefe = b.getTiefe();
        zeigerSort = 0;
    }

    public BinaerBaum getBaum(){
        return b;
    }
    
    public int[] getUnsortiert(){
        return unsortiert;
    }
    
    public int[] getSortiert(){
        return sortiert;
    }

    private int wettkampf(int pos){
        if(b.getInhalt(pos)==-1 && b.getInhalt(pos+1)==-1) return -1;     // beide leer --> -1
        if(b.getInhalt(pos)>-1 && b.getInhalt(pos+1)==-1){        // rechts leer
            b.schiebeSohnAufVaterPos(pos);
            return pos;
        }
        if(b.getInhalt(pos)==-1 && b.getInhalt(pos+1)>-1){        // links leer
            b.schiebeSohnAufVaterPos(pos+1);
            return pos+1;
        }
        if(b.getInhalt(pos) <= b.getInhalt(pos+1)) {
            b.schiebeSohnAufVaterPos(pos);
            return pos;
        }
        else {
            b.schiebeSohnAufVaterPos(pos+1);
            return pos+1;
        }
    }

    private void nachruecken(int pos){
        // schiebe Elemente iterativ von unten nach oben nach, falls frei
        while(pos != -1 && !b.istBlattPos(pos)){     // solange man noch nicht am Blatt angekommen ist
            if(b.getInhalt(pos)==-1){      //oben frei
                int i = b.getLinkerSohnPos(pos);    // gehe zum linken Sohn
                pos = wettkampf(i);
            }
        }
        // am Blatt angekommen -- Ende
    }

    private void setWert(int index, int wert) {
        if(index < sortiert.length)  sortiert[index] = wert;
    }

    private void raus(){
        // Oberstes Element in das Array sortiert schreiben
        if(b.getInhalt(0)>-1){
            setWert(zeigerSort, b.getInhalt(0));
            zeigerSort++;
            b.setInhalt(0,-1);
        }
    }

    public void sort(){
        start();
        init();
        //# Hier kommt dein Sortieralgorithmus hin.
        //# Speichere deine sortierte Liste im Array sortiert:
        int anfang;
        int ende;
        while(!b.isLeer()){   // Baum ist nicht leer
            anfang = (int)Math.pow(2,aktTiefe)-1;
            ende = (int)Math.pow(2,aktTiefe+1)-2;
            // Wettkampf - gehe alle Elemente der aktTiefe durch
            for(int i=anfang; i<ende; i=i+2){       // es wird aufsteigend sortiert
                int pos = wettkampf(i);
                nachruecken(pos);
            }
            raus();
            if(aktTiefe>1) aktTiefe--;
        }
        ausgabe();
        stop();
    }

    public void ausgabe(){
        if(b.getZahlen().length<10000){
            unsortiert = b.getZahlen();
            System.out.println("Unsortiert: "+java.util.Arrays.toString(unsortiert)+"\nSortiert: "+java.util.Arrays.toString(sortiert));
        }
        else {
            System.out.println("Array zu groß für eine Ausgabe!");
        }
    }
}