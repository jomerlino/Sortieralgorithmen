import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

/**
 * Die Test-Klasse BinaerBaumTest.
 *
 * @author  (Ihr Name)
 * @version (eine Versionsnummer oder ein Datum)
 */
public class BinaerBaumTest
{
    private int[] intArray5;
    private int[] intArray2;
    private int[] intArray10;
    private BinaerBaum binaerba1;
    private int[] baum2;
    private int[] baum5;
    private int[] baum10;

    /**
     * Konstruktor fuer die Test-Klasse BinaerBaumTest
     */
    public BinaerBaumTest()
    {
    }

    /**
     *  Setzt das Testgerüst fuer den Test.
     *
     * Wird vor jeder Testfall-Methode aufgerufen.
     */
    @BeforeEach
    public void setUp()
    {
        int[] zahlen_5 = {4,3,7,2,8};
        intArray5 = zahlen_5;
        int[] zahlen_2 = {4,7};
        intArray2 = zahlen_2;
        int[] zahlen_10 = {4,7,9,2,5,3,6,7,1,4};
        intArray10 = zahlen_10;
        binaerba1 = new BinaerBaum();
        // binaerba1.fuelleBlaetterMitArray(intArray10);
        // binaerba1.ausgabe();
        // binaerba1.schiebeSohnAufVaterPos(18);
        // binaerba1.ausgabe();
    }

    /**
     * Gibt das Testgerüst wieder frei.
     *
     * Wird nach jeder Testfall-Methode aufgerufen.
     */
    @AfterEach
    public void tearDown()
    {
    }

    @Test
    public void testeFuelleBaum()
    {
        binaerba1.fuelleBlaetterMitArray(intArray2);
        baum2 = binaerba1.getBaum();
        binaerba1.ausgabe();
        int[] korrekterBaum2 = {-1,4,7};
        assertTrue(Arrays.equals(baum2, korrekterBaum2));
        System.out.println("Der mit zwei Zahlen populierte Baum ist korrekt!");

        binaerba1.fuelleBlaetterMitArray(intArray5);
        baum5 = binaerba1.getBaum();
        binaerba1.ausgabe();
        int[] korrekterBaum5 = {-1,-1,-1,-1,-1,-1,-1,4,3,7,2,8,-1,-1,-1};
        assertTrue(Arrays.equals(baum5, korrekterBaum5));
        System.out.println("Der mit 5 Zahlen populierte Baum ist korrekt!");

        binaerba1.fuelleBlaetterMitArray(intArray10);
        baum10 = binaerba1.getBaum();
        binaerba1.ausgabe();
        int[] korrekterBaum10 = {-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,4,7,9,2,5,3,6,7,1,4,-1,-1,-1,-1,-1,-1};
        assertTrue(Arrays.equals(baum10, korrekterBaum10));
        System.out.println("Der mit 10 Zahlen populierte Baum ist korrekt!");
    }

    @Test
    public void testeVaterPosition()
    {
        binaerba1.fuelleBlaetterMitArray(intArray10);
        baum10 = binaerba1.getBaum();
        assertEquals(-1, binaerba1.getVaterPos(0));
        for(int i=1; i<baum10.length; i++){
            assertEquals((int)((i-1)/2), binaerba1.getVaterPos(i));
        }
    }
}
