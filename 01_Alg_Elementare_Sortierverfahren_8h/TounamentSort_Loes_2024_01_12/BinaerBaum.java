public class BinaerBaum
{
    private int[] baum;
    private int[] zahlen;
    private int tiefe;

    public BinaerBaum(){
        baum = null;
        tiefe = 0;
    }

    private void setzeGroesseAuf(int anzahl){
        baum = new int[anzahl];
        for(int i=0; i<anzahl; i++){
            baum[i]=-1;
        }
    }

    public void fuelleBlaetterMitArray(int[] zahlen){
        int n = zahlen.length;
        this.zahlen = zahlen;
        tiefe = (int)Math.ceil((Math.log(n)/Math.log(2)));
        int baumAnzahl = (int)Math.pow(2,tiefe+1)-1;
        setzeGroesseAuf(baumAnzahl);
        int offset = (int)baumAnzahl/2;
        for(int i=0; i<n; i++){
            baum[offset+i] = zahlen[i];
        }
    }

    public int getInhalt(int pos){
        return baum[pos];
    }

    public void setInhalt(int pos, int wert){
        baum[pos]=wert;
    }

    public int getTiefe(){
        return tiefe;
    }
    
    public int[] getZahlen(){
        return zahlen;
    }

    /**
     * Gibt -1, falls kein Vater d.h. WurzelPos als SohnPos uebergeben
     */
    public int getVaterPos(int sohnPos){
        if(sohnPos <= 0) return -1;
        return (int)((sohnPos-1)/2);
    }

    /**
     * Gibt -1, falls kein Vater d.h. WurzelPos als SohnPos uebergeben
     */
    public int getInhaltAnVaterPos(int sohnPos){
        if(sohnPos <= 0) return -1;
        return baum[getVaterPos(sohnPos)];
    }

    /**
     * Setzt den SohnInhalt nach dem verschieben auf 0 (bedeutet leer)
     */
    public boolean schiebeSohnAufVaterPos(int sohnPos){
        if(baum[sohnPos]==-1 || getInhaltAnVaterPos(sohnPos)!=-1) return false;
        else {
            baum[getVaterPos(sohnPos)] = baum[sohnPos];
            baum[sohnPos]=-1;
            return true;
        }
    }

    public boolean isLeer(){
        for(int i=0; i<baum.length; i++){
            if(baum[i] != -1) return false;
        }
        return true;
    }

    /**
     * Gibt -1, falls kein Sohn d.h. Blatt als vaterPos uebergeben
     */
    public int getLinkerSohnPos(int vaterPos){
        if (istBlattPos(vaterPos) || vaterPos >= baum.length) return -1;
        else return (vaterPos*2+1);
    }

    public boolean istBlattPos(int pos){
        if (pos >= baum.length) return false;
        else return (pos >= (int)(Math.pow(2,tiefe)-1) && pos < (int)(Math.pow(2,tiefe+1)-1));
    }
    
    public int[] getBaum(){
        return baum;
    }

    public void ausgabe(){
        String s="Binaerbaum:\n";
        if(baum.length>0)s+=baum[0]+"\n";
        for(int i=1; i<baum.length; i++){
            s+=baum[i]+"\t";
            double d = (Math.log(i+2)/Math.log(2));
            int n = (int)d;
            if (d>0 && (d - n) == 0.0) {
                s+="\n";
            }
        }
        s+="\n";
        System.out.println(s);
    }
}