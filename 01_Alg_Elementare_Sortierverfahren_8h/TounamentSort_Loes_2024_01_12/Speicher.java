import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * Die Test-Klasse Speicher.
 *
 * @author  (Ihr Name)
 * @version (eine Versionsnummer oder ein Datum)
 */
public class Speicher
{
    private int[] intArray2;
    private int[] intArray5;
    private int[] intArray10;
    private TournamentSort tourname1;
    private BinaerBaum binaerBa1;

    /**
     * Konstruktor fuer die Test-Klasse Speicher
     */
    public Speicher()
    {
    }

    /**
     *  Setzt das Testgerüst fuer den Test.
     *
     * Wird vor jeder Testfall-Methode aufgerufen.
     */
    @BeforeEach
    public void setUp()
    {
        intArray2 = new int[] {4,7};
        intArray5 = new int[] {4,3,7,2,8};
        intArray10 = new int[] {4,7,9,2,5,3,6,7,1,4};
        tourname1 = new TournamentSort(intArray10);
        binaerBa1 = tourname1.getBaum();
        tourname1.ausgabe();
        binaerBa1.ausgabe();
    }

    /**
     * Gibt das Testgerüst wieder frei.
     *
     * Wird nach jeder Testfall-Methode aufgerufen.
     */
    @AfterEach
    public void tearDown()
    {
    }
}