public class TournamentSort
{
    private BinaerBaum b;
    private int[] unsortiert;
    private int[] sortiert;
    private int aktTiefe;

    public TournamentSort(){
        this(new int[]{4,3,7,2,8});
    }
    
    public TournamentSort(int[] neueZahlen){
        b = new BinaerBaum();
        unsortiert = neueZahlen;
        b.fuelleBlaetterMitArray(unsortiert);
        sortiert = new int[neueZahlen.length];
        aktTiefe = b.getTiefe();
    }
    
    public BinaerBaum getBaum(){
        return b;
    }

    public void sort(){
        //# Hier kommt dein Sortieralgorithmus hin.
        //# Speichere deine sortierte Liste im Array sortiert:
        
    }
    
    public void ausgabe(){
        System.out.println("Unsortiert: "+java.util.Arrays.toString(unsortiert)+"\nSortiert: "+java.util.Arrays.toString(sortiert));
    }
}