import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.File;
import java.io.FileWriter;
import com.csvreader.CsvReader;
import com.csvreader.CsvWriter;

/**
 * Beschreiben Sie hier die Klasse CSV_Import_Export.
 * 
 * @author Dirk Zechnall
 * @version 11.05.2011
 */
public class CSV_Import_Export
{
    /*
     * Methoden f�r Objekte der Klasse CSV_Import_Export
     */
    /**
     * readCSV
     */
    // File Format
    // ProductID,ProductName
    // 1,Chai
    // 2,Chang

    public void readCSV(String dateiName) {
        try {

            CsvReader datei = new CsvReader("export/"+dateiName);

            datei.readHeaders();

            while (datei.readRecord())
            {
                String productID = datei.get("ProductID");
                String productName = datei.get("ProductName");

                // perform program logic here
                System.out.println(productID + ": " + productName);
            }

            datei.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /**
     * writeCSV
     */
    //     Output Format
    // 
    //     id,name
    //     1,Bruce
    //     2,John
    public void writeCSV(String dateiName, String[] attribute, String[] datensatz, boolean append) {
        // before we open the file check to see if it already exists
        boolean alreadyExists = new File("export/"+dateiName).exists();

        try {
            // use FileWriter constructor that specifies open for appending
            CsvWriter csvOutput = new CsvWriter(new FileWriter("export/"+dateiName, append), ',');

            // if the file didn't already exist then we need to write out the header line
            if (!alreadyExists)
            {
                for (String item: attribute) csvOutput.write(item);
                csvOutput.endRecord();
            }
            // else assume that the file already has the correct header line

            // write out a few records
            for (String item: datensatz) csvOutput.write(item);
            csvOutput.endRecord();

            csvOutput.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}