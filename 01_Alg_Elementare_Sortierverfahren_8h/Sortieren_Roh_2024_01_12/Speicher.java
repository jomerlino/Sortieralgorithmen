import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

/**
 * Die Test-Klasse Speicher.
 *
 * @author  Dirk Zechnall
 * @version 11.01.2024
 */
public class Speicher
{
    private Testroutine testBubble;
    private Testroutine testSelect;
    private Testroutine testInsert;
    private Testroutine testTournament;
    private BubbleSort bubble;
    private InsertSort insert;
    private SelectSort select;

    private double[] sorted;

    final int MAX = 50000;

    /**
     * Konstruktor fuer die Test-Klasse Speicher
     */
    public Speicher()
    {
    }

    /**
     *  Setzt das Testgerüst fuer den Test.
     *
     * Wird vor jeder Testfall-Methode aufgerufen.
     */
    @BeforeEach
    public void setUp()
    {
        bubble = new BubbleSort();
        insert = new InsertSort();
        select = new SelectSort();
        testBubble = new Testroutine(bubble);
        testInsert = new Testroutine(insert);
        testSelect = new Testroutine(select);
    }

    /**
     * Gibt das Testgerüst wieder frei.
     *
     * Wird nach jeder Testfall-Methode aufgerufen.
     */
    @AfterEach
    public void tearDown()
    {
    }

    @Test
    public void testeSelectionSort()
    {
        select.setGarKeineAusgabe(true);
        double[] unsorted;

        // unsortiert
        for (int i=10000; i<=MAX; i=i+10000){
            select.feldErzeugenZufaellig(i,i);
            unsorted = select.getArray();
            sorted = Arrays.copyOf(unsorted, unsorted.length);
            Arrays.sort(sorted);
            select.sort();
            assertTrue(Arrays.equals(sorted,select.getArray()));
            System.out.println("SelectionSort Test: Das zufaellig generierte Array mit "+i+" Elementen wurde in "+select.getDauer()+" ms korrekt sortiert!");
        }
        System.out.println("SelectionSort Test: Alle zufaellig generierten Arrays wurden korrekt sortiert!");

        // aufsteigend
        select.feldErzeugenAufsteigend(MAX);
        unsorted = select.getArray();
        sorted = Arrays.copyOf(unsorted, unsorted.length);
        Arrays.sort(sorted);
        select.sort();
        assertTrue(Arrays.equals(sorted,select.getArray()));
        System.out.println("SelectionSort Test: Das aufsteigend vorsortierte Array wurde in "+select.getDauer()+" ms korrekt sortiert!");

        // absteigend
        select.feldErzeugenAbsteigend(MAX);
        unsorted = select.getArray();
        sorted = Arrays.copyOf(unsorted, unsorted.length);
        Arrays.sort(sorted);
        select.sort();
        assertTrue(Arrays.equals(sorted,select.getArray()));
        System.out.println("SelectionSort Test: Das absteigend vorsortierte Array wurde in "+select.getDauer()+" ms korrekt sortiert!");

        select.setGarKeineAusgabe(false);
    }

    @Test
    public void testeBubbleSort()
    {
        bubble.setGarKeineAusgabe(true);

        double[] unsorted;

        // unsortiert
        for (int i=10000; i<=MAX; i=i+10000){
            bubble.feldErzeugenZufaellig(i,i);
            unsorted = bubble.getArray();
            sorted = Arrays.copyOf(unsorted, unsorted.length);
            Arrays.sort(sorted);
            bubble.sort();
            assertTrue(Arrays.equals(sorted,bubble.getArray()));
            System.out.println("BubbleSort Test: Das zufaellig generierte Array mit "+i+" Elementen wurde in "+bubble.getDauer()+" ms korrekt sortiert!");
        }
        System.out.println("BubbleSort Test: Alle zufaellig generierten Arrays wurden korrekt sortiert!");

        // aufsteigend
        bubble.feldErzeugenAufsteigend(MAX);
        unsorted = bubble.getArray();
        sorted = Arrays.copyOf(unsorted, unsorted.length);
        Arrays.sort(sorted);
        bubble.sort();
        assertTrue(Arrays.equals(sorted,bubble.getArray()));
        System.out.println("BubbleSort Test: Das aufsteigend vorsortierte Array wurde in "+bubble.getDauer()+" ms korrekt sortiert!");

        // absteigend
        bubble.feldErzeugenAbsteigend(MAX);
        unsorted = bubble.getArray();
        sorted = Arrays.copyOf(unsorted, unsorted.length);
        Arrays.sort(sorted);
        bubble.sort();
        assertTrue(Arrays.equals(sorted,bubble.getArray()));
        System.out.println("BubbleSort Test: Das absteigend vorsortierte Array wurde in "+bubble.getDauer()+" ms korrekt sortiert!");

        bubble.setGarKeineAusgabe(false);
    }

    @Test
    public void testeInsertSort()
    {
        insert.setGarKeineAusgabe(true);

        double[] unsorted;

        // unsortiert
        for (int i=10000; i<=MAX; i=i+10000){
            insert.feldErzeugenZufaellig(i,i);
            unsorted = insert.getArray();
            sorted = Arrays.copyOf(unsorted, unsorted.length);
            Arrays.sort(sorted);
            insert.sort();
            assertTrue(Arrays.equals(sorted,insert.getArray()));
            System.out.println("InsertionSort Test: Das zufaellig generierte Array mit "+i+" Elementen wurde in "+insert.getDauer()+" ms korrekt sortiert!");
        }
        System.out.println("InsertionSort Test: Alle zufaellig generierten Arrays wurden korrekt sortiert!");

        // aufsteigend
        insert.feldErzeugenAufsteigend(MAX);
        unsorted = insert.getArray();
        sorted = Arrays.copyOf(unsorted, unsorted.length);
        Arrays.sort(sorted);
        insert.sort();
        assertTrue(Arrays.equals(sorted,insert.getArray()));
        System.out.println("InsertionSort Test: Das aufsteigend vorsortierte Array wurde in "+insert.getDauer()+" ms korrekt sortiert!");

        // absteigend
        insert.feldErzeugenAbsteigend(MAX);
        unsorted = insert.getArray();
        sorted = Arrays.copyOf(unsorted, unsorted.length);
        Arrays.sort(sorted);
        insert.sort();
        assertTrue(Arrays.equals(sorted,insert.getArray()));
        System.out.println("InsertionSort Test: Das absteigend vorsortierte Array wurde in "+insert.getDauer()+" ms korrekt sortiert!");

        insert.setGarKeineAusgabe(false);
    }
}