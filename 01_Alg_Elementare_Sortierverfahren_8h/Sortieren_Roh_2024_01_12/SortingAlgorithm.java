/**
 * @author Dirk Zechnall
 * @version 11.01.2024
 */
public abstract class SortingAlgorithm
{
    protected Uhr stoppUhr;
    protected String name;
    protected double[] array;
    private boolean konsolenAusgabe;
    private boolean garKeineAusgabe;
    private long vertauschungen;
    private long vergleiche;

    /**
     * Im Konstruktor werden mit der Anzahl der Elemente die Groesse des
     * Feldes festgelegt.
     * @param int anzahlElemente: Die Anzahl der zu sortierenden Zahlen
     */
    public SortingAlgorithm(){
        this(100);
    }

    public SortingAlgorithm(int anzahlElemente)
    {
        stoppUhr = new Uhr();
        name = "";
        feldErzeugenZufaellig(anzahlElemente,anzahlElemente);
        if(anzahlElemente<=1000) konsolenAusgabe = true;
        else konsolenAusgabe = false;
        garKeineAusgabe = false;
    }

    /**
     * Methoden - Faehigkeiten von Objekten der Klasse Feld 
     */
    /*# Aufgaben: Implementiere Hilfsmethoden fuer das Sortieren*/
    public boolean istInhaltKleiner(int index1, int index2) {
        vergleiche++;
        //# Aufgabe 1: Implementiere hier den Vergleich auf Kleiner

        return true;
    }

    public void tauscheInhalteAnPos(int index1, int index2){
        //# Aufgabe 2: Implementiere hier das Tauschen der Inhalte an den gegebenen Positionen:

        vertauschungen++;
    }
    
    
    public abstract void sort();

    protected void start(){
        if(!garKeineAusgabe && konsolenAusgabe) {
            System.out.println("Das noch unsortierte Feld sieht so aus:");
            feldKonsolenAusgabe();
        }
        resetZaehlung();
        stoppUhr.start();
    }

    protected void stop() {
        stoppUhr.stop();
        if(!garKeineAusgabe && konsolenAusgabe) {
            System.out.println("Das sortierte Feld sieht wie folgt aus:");
            feldKonsolenAusgabe();
        }
        if(!garKeineAusgabe) System.out.println("Die Sortierung von "+getLaenge()+" Elementen mit "+name+" dauerte "+stoppUhr.getDauer()+" Millisekunden und benötigte "+getAnzahlVergleiche()+" Vergleiche und "+getAnzahlVertauschungen()+" Vertauschungen.\n");
    }

    public void feldErzeugenZufaellig(int anzahl, int bereich){
        resetZaehlung();
        if (anzahl < 0) anzahl = Integer.MAX_VALUE;
        if (bereich < 0) bereich = Integer.MAX_VALUE;
        array = new double[anzahl];
        for (int i=0; i<array.length; i++){
            array[i]=(bereich*Math.random());
        }
    }

    public void feldErzeugenAufsteigend(int anzahl){
        resetZaehlung();
        array = new double[anzahl];
        for (int i=0; i<array.length; i++){
            array[i]=i;
        }
    }

    public void feldErzeugenAbsteigend(int anzahl){
        resetZaehlung();
        array = new double[anzahl];
        for (int i=0; i<array.length; i++){
            array[i]=anzahl-i;
        }
    }

    private void resetZaehlung() {
        vergleiche = 0;
        vertauschungen = 0;
    }

    /*# Methode fuer die Konsolenausgabe */
    public void feldKonsolenAusgabe () {
        System.out.println("Array der Laenge: "+array.length);
        System.out.print("Index :  ");
        for (int i=0; i<array.length; i++) {
            System.out.print(String.format("|%5d|",i));
        }
        System.out.println("");
        System.out.print("Inhalt:  ");
        for (int i=0; i<array.length; i++) {
            System.out.print(String.format("|%5.2f|",Math.round(array[i]*100.0)/100.0));
        }
        System.out.println("\n");
    }

    /*# getter und setter */
    public double[] getArray() {
        return array;
    }

    public int getLaenge() {
        return array.length;
    }

    public long getAnzahlVergleiche() {
        return vergleiche;
    }

    public long getAnzahlVertauschungen() {
        return vertauschungen;
    }

    public long getDauer ()
    {
        return stoppUhr.getDauer();
    }

    public String getName ()
    {
        return name;
    }

    public double getWert(int index) {
        return array[index];
    }

    public void setWert(int index, double wert) {
        if(index < array.length)  array[index] = wert;
    }

    public void setKonsolenausgabe(boolean ausgabe){
        konsolenAusgabe = ausgabe;
    }

    public void setGarKeineAusgabe(boolean ausgabe){
        garKeineAusgabe = ausgabe;
    }
}