/**
 * @author Dirk Zechnall
 * @version 11.05.2011
 */
public class InsertSort extends SortingAlgorithm
{
    /**
     * Im Konstruktor werden mit der Anzahl der Elemente die Groesse des
     * Feldes festgelegt.
     * @param int anzahlElemente: Die Anzahl der zu sortierenden Zahlen
     */
    public InsertSort()
    {
        super();
        name = "InsertSort";
    }

    /**
     * Methoden - Fähigkeiten von Objekten der Klasse Feld 
     */
    public void sort(){
        start();
        /*#
         * Aufgaben:
         * 1)  Implementiere in der Methode "public void sort()" den InsertionSort-Algorithmus.
         * 2)  Erzeuge mind. zwei Objekte der Klasse und vergleiche in den Inspect-Fenstern
         *     die Felder und die Sortierdauer vor und nach dem Sortieren
         */
        
        
        stop();
    }
}